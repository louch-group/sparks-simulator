# Sparks simulator

This project is dedicated to simulating calcium sparks. For that, a set of diffusion-reaction equations are solved using the finite element method.

The Python code in this project is developed and tested using the following packages:
```
package     version
Python      3.6.9
numpy       1.16.4
scipy       1.3.0
h5py        2.7.1
fenics-ufl  2019.2.0.dev0
mshr        2019.2.0.dev0
matplotlib  3.1.0
argparse    1.1
```

Here is a short guide to get started:

First, it is necessary to calculate average RyR calcium fluxes. For that, use script
`ryr_flux_sticky_cluster_model.py` in thr subfolder `model`. Run `python
ryr_flux_sticky_cluster_model.py -h` to see further information about
the script usage.

The output of `ryr_flux_sticky_cluster_model.py` is used as an input of the FEM model (`model.py`) for calculating spatio-temporal changes in calcium and calcium buffers concentrations. However, before using the script, a FE mesh should be generated using `generate_mesh.py`.

The results of the FEM model are converted to "experimental" data using the script `simulation_data_reader.py`. This  integrates calcium-bound fluorescence signal to match exposure time and convolves it with proper point spread function. Run `python simulation_data_reader.py -h` to see further information about the script.
