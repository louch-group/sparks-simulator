import matplotlib.font_manager as font_manager


def plot_conf():
    lw = 1.5
    fs = 14
    mpad = 8
    mpad_x = 5
    allwidth = 1.5
    d = {
        'axes.linewidth': allwidth,
        'axes.spines.top': False,
        'axes.spines.right': False,

        #'text.usetex': True,
        'text.latex.preamble': [r"\usepackage{wasysym}",
                                r"\everymath{\sf}",
                                r"\renewcommand{\rmdefault}{\sfdefault}"],
        #'text.latex.preview': False,

        'figure.subplot.bottom': 0.15,
        'figure.subplot.hspace': 0.2,
        'figure.subplot.left': 0.15,
        'figure.subplot.right': 0.95,
        'figure.subplot.top': 0.95,
        'figure.subplot.wspace': 0.2,

        'font.size': fs,
        'font.weight': 'normal',
        'font.family': 'sans-serif',
        #'font.sans-serif': ['Latin Modern Sans'],

        'legend.fontsize': 10,
        'legend.frameon': False,

        'xtick.direction': 'in',
        'xtick.labelsize': 'medium',
        'xtick.major.pad': mpad_x,
        'xtick.major.size': 5,
        'xtick.major.width': allwidth,
        'xtick.major.top': False,
        'xtick.minor.pad': mpad,
        'xtick.minor.size': 3,
        'xtick.minor.width': allwidth,

        'ytick.direction': 'in',
        'ytick.labelsize': 'medium',
        'ytick.major.pad': mpad,
        'ytick.major.size': 5,
        'ytick.major.width': allwidth,
        'ytick.major.right': False,
        'ytick.minor.pad': mpad,
        'ytick.minor.size': 3,
        'ytick.minor.width': allwidth,}

    return d


# Usage
# plt.rcParams.update(plot_conf())
