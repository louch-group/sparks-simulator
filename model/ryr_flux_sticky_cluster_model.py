#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2019-2022
#  Institute for Experimental Medical Research,
#  Oslo University Hospital and University of Oslo, Oslo, Norway
#  Authors: Yufeng Hou and Martin Laasmaa
#  This file is part of project: Sparks-Simulator
#
#
# Model is based on publication
# Parameter Sensitivity Analysis of Stochastic Models Provides Insights into Cardiac Calcium Sparks
# by Young-Seon Lee, Ona Z. Liu, Hyun Seok Hwang, Bjorn C. Knollmann, and Eric A. Sobie
# in Biophysical Journal, Volume 104, March 2013
# http://dx.doi.org/10.1016/j.bpj.2012.12.055

import numpy as np
import pylab as plt
import os
import h5py

from scipy.integrate import simps, ode
from plot_style import *
plt.rcParams.update(plot_conf())


def model(N_tot=28, show=False):
    # Constants:
    V_SS = 1.0e-12 # uL
    V_JSR = 1.6e-12 # uL
    I_DHPR = 0
    tau_efflux = 1.78e-3 # ms
    tau_refill = 6.5 # ms

    D_RyR = 2.2e-12 #uL/ms
    k_coop = 1 # unitless
    k_open_max = 30 # 1/ms
    k_close_max = 0.480 # 1/ms
    alpha = 1.0e-3 # unitless
    K_max = 19.87 # uM

    Ej = 0.1
    K_coup = np.exp(2*Ej/(N_tot-1))

    Ca_NSR = 1.0e3 # uM
    Ca_myo = 0.1 #uM

    Ca_B_SR_tot = 47 # uM
    k_on_SR = 0.115 # 1/uM/ms
    k_off_SR = 0.100 # 1/ms

    Ca_B_CaM_tot = 24 # uM
    k_on_CaM = 0.100 # 1/uM/ms
    k_off_CaM = 0.038 # 1/ms

    Ca_B_SL_tot = 900 # uM
    k_on_SL = 0.115 # 1/uM/ms
    k_off_SL = 1.000 # 1/ms

    CSQ_tot = 30.0e3 # uM
    K_CSQ = 630 #  uM

    alg_labels = 'J_DHPR', 'J_release', 'J_RyR', 'J_buf', 'J_efflux', 'J_refill', 'beta_JSR', \
                 'N_closed', 'CF_open', 'K_m', 'k_close', 'k_open', 'p_close', 'p_open', 'N_open'
    states_labels = 'Ca_SS', 'Ca_B_SR', 'Ca_B_CaM', 'Ca_B_SL', 'Ca_lumen'

    def calculate_free_buffer(ca, on, off, tot):
        return on*ca*tot/(on*ca+off)

    def initial_states(Ca_myo):
        # Initial states:
        Ca_lumen = 1.0e3 # uM
        Ca_SS = Ca_myo#0.1 #uM
        Ca_B_SR = calculate_free_buffer(Ca_SS, k_on_SR, k_off_SR, Ca_B_SR_tot)
        Ca_B_CaM = calculate_free_buffer(Ca_SS, k_on_CaM, k_off_CaM, Ca_B_CaM_tot)
        Ca_B_SL = calculate_free_buffer(Ca_SS, k_on_SL, k_off_SL, Ca_B_SL_tot)
        return Ca_SS, Ca_B_SR, Ca_B_CaM, Ca_B_SL, Ca_lumen

    def algebraic(concs):
        Ca_SS, Ca_B_SR, Ca_B_CaM, Ca_B_SL, Ca_lumen = concs

        # Component: Subspace
        # J_DHPR = -I_DHPR/2.0/F/V_SS
        J_DHPR = 0
        J_RyR = D_RyR/V_SS*(Ca_lumen - Ca_SS)
        J_release = N_open*J_RyR

        J_CaM = k_on_CaM*Ca_SS*(Ca_B_CaM_tot - Ca_B_CaM) - k_off_CaM*Ca_B_CaM
        J_SR = k_on_SL*Ca_SS*(Ca_B_SL_tot - Ca_B_SL) - k_off_SL*Ca_B_SL
        J_SL = k_on_SR*Ca_SS*(Ca_B_SR_tot - Ca_B_SR) - k_off_SR*Ca_B_SR
        J_buf = J_CaM + J_SR + J_SL

        J_efflux = (Ca_myo - Ca_SS)/tau_efflux
        J_refill = (Ca_NSR - Ca_lumen)/tau_refill
        beta_JSR = 1/(1.0 + CSQ_tot*K_CSQ / (Ca_lumen+K_CSQ)**2.0)

        # RyR gaiting
        N_closed = N_tot - N_open

        CF_open = np.power(K_coup, 2*N_open + 1 - N_tot)
        CF_close = np.power(K_coup, 2*N_closed + 1 - N_tot)
        K_m = K_max - alpha*Ca_lumen
        k_close = CF_close*k_close_max
        k_open = CF_open*k_open_max * Ca_SS**4/(Ca_SS**4 + K_m**4)
        p_close = dt*k_close #*N_open
        p_open = dt*k_open #*N_closed

        #print('pclose, popen', N_open, p_close, p_open)
        nnc = np.random.choice([0, 1], N_closed, p=[1-p_open, p_open]) #
        nno = np.random.choice([0, 1], N_open, p=[1-p_close, p_close]) #
        new_n_open = N_open - nno.sum() + nnc.sum()

        return J_DHPR, J_release, J_RyR, J_buf, J_efflux, J_refill, beta_JSR, N_closed, CF_open, K_m, k_close, k_open, p_close, p_open, new_n_open

    def computeRates(t, concs):
        # Rates
        Ca_SS, Ca_B_SR, Ca_B_CaM, Ca_B_SL, Ca_lumen = concs
        J_DHPR, J_release, J_RyR, J_buf, J_efflux, J_refill, beta_JSR, N_closed, CF_open, K_m, k_close, k_open, p_close, p_open, new_n_open = algebraic(concs)
        ddtCa_SS = J_release + J_DHPR + J_efflux + J_buf
        ddtCa_lumen = beta_JSR*(-J_release*V_SS/V_JSR + J_refill)
        ddtCa_B_CaM = k_on_CaM*Ca_SS*(Ca_B_CaM_tot - Ca_B_CaM) - k_off_CaM*Ca_B_CaM
        ddtCa_B_SL = k_on_SL*Ca_SS*(Ca_B_SL_tot - Ca_B_SL) - k_off_SL*Ca_B_SL
        ddtCa_B_SR = k_on_SR*Ca_SS*(Ca_B_SR_tot - Ca_B_SR) - k_off_SR*Ca_B_SR
        return [ddtCa_SS, ddtCa_B_SR, ddtCa_B_CaM, ddtCa_B_SL, ddtCa_lumen]

    def solve_model(show=False):
        """ Solve model with ODE solver """
        # from scipy.integrate import ode
        # Initialise constants and state variables
        init_concs = initial_states(Ca_myo)

        # Set timespan to solve over
        global dt
        dt = 1.e-3
        voi = np.arange(0, 25, dt)
        global N_open
        N_open = 0
        algebraic(init_concs)
        # Construct ODE object to solve
        r = ode(computeRates)
        r.set_integrator('lsoda', atol=1e-09, rtol=1e-09, max_step=dt)
        r.set_initial_value(init_concs, voi[0])

        size_states = 5
        size_algebraic = 15
        # Solve model
        states = np.array([[0.0] * len(voi)] * size_states)
        algebraic_t = np.array([[0.0] * len(voi)] * size_algebraic)

        states[:,0] = init_concs
        algebraic_t[:,0] = algebraic(init_concs)
        N_open = 0
        for (i,t) in enumerate(voi[1:]):
            if r.successful():
                if i==100:
                    N_open = 1
                r.integrate(t)
                states[:,i+1] = r.y
                vals = algebraic(r.y)
                algebraic_t[:,i+1] = vals
                N_open = vals[-1]
            else:
                break

        if show:
            fig = plt.figure()
            ax3 = fig.add_subplot(313)

            for i, a in enumerate(algebraic_t[1:7]):
                ax1 = fig.add_subplot(3,6,1+i)
                ax1.plot(voi, a, label=alg_labels[1:7][i])
                ax1.legend(frameon=0)
            ax3.plot(voi, algebraic_t[-1,:])

            for i, s in enumerate(states):
                ax2 = fig.add_subplot(3,5,6+i)
                ax2.plot(voi, s, label=states_labels[i])
                ax2.legend(frameon=0)
            plt.show()
        return voi, states, states_labels, algebraic_t, alg_labels

    return solve_model(show)


class SimReader(object):

    def __init__(self, filename):
        self.filename = filename
        self.states = None
        self.algebraic = None
        self.time = None
        self._load()

    def _load(self):
        h5 = h5py.File(self.filename, 'r')

        self.time = h5['time'][()]
        states = h5['states'][()]
        states_labels = eval(h5['states'].attrs['labels'])
        self.states = {key: states[i] for i, key in enumerate(states_labels)}

        algebraic = h5['algebraic_t'][()]
        algebraic_labels = eval(h5['algebraic_t'].attrs['labels'])
        self.algebraic = {key: algebraic[i] for i, key in enumerate(algebraic_labels)}

        h5.close()

    def __getitem__(self, label):
        if label in self.states:
            return self.states[label]
        if label in self.algebraic:
            return self.algebraic[label]
        if label == 'time':
            return self.time
        raise KeyError('Existing keys are: time',
                       ['time']+list(self.states.keys())+list(self.algebraic.keys()))


def average_flux(N_tot, path):

    fig = plt.figure(figsize=(8,8))
    fig.subplots_adjust(left=0.11, bottom=0.17, right=0.98, top=0.97, wspace=0.3, hspace=None)
    ax3 = fig.add_subplot(221)
    ax1 = fig.add_subplot(222)
    ax4 = fig.add_subplot(223)
    ax2 = fig.add_subplot(224)

    n0, n1 = 0, 100
    c = 0
    lim = N_tot*0.8
    max_coord = []
    mean_nopen = []

    for i in range(n0, n1):
        fn = os.path.join(path, str(N_tot), '%05d.h5'%i)
        print(fn)
        sim = SimReader(fn)
        nopen = sim['N_open']
        mean_nopen.append(nopen)

        ax3.plot(sim['time'], nopen, alpha=0.25, color='k')
        if 1:
            ax4.plot(sim['time'], 0.001*sim['Ca_lumen'], alpha=0.25, color='k')
            ax2.plot(sim['time'], sim['Ca_SS'], alpha=0.25, color='k')

        J_release = -sim['J_efflux']
        ax1.plot(sim['time'], 0.001*J_release, alpha=0.25, color='k')
        if sim['N_open'].max() < lim:
            continue
        c += 1

        max_coord.append(J_release.argmax())

    Ca_lumen = 1
    ax4.set_ylim([-0.05*Ca_lumen, 1.05*Ca_lumen])
    ax4.set_ylabel('[Ca$^{2+}$] jSR, mM')

    Ca_SS = 200
    ax2.set_ylim([-0.05*Ca_SS, 1.05*Ca_SS])
    ax2.set_ylabel('[Ca_SS], mM')

    nopen_mean = np.mean(mean_nopen, axis=0)
    nopen_std = np.std(mean_nopen, axis=0)
    nopen_se = nopen_std/np.sqrt(n1-n0)

    upper = nopen_mean + nopen_std
    upper = np.where(upper > N_tot, N_tot, upper)
    lower = nopen_mean - nopen_std
    lower = np.where(lower < 0, 0, lower)
    ax3.fill_between(sim['time'], lower, upper, color='b', alpha=0.3)
    ax3.plot(sim['time'], nopen_mean, color='b', lw=2, label='Average opening')

    peak_time = np.percentile(max_coord, 25)
    print('USING NUM OPEN RYRs:', lim)
    print('USING PEAK TIME:', peak_time)
    print(f'FRACTION OF SIMULATIONS WHERE ATLEAST {lim} RyR was opened:', c/(n1-n0))

    ax3.set_ylabel('Number of open RyRs')
    ax1.set_ylabel('Ca$^{2+}$ efflux (mM/ms)')

    for ax in [ax1, ax3]:
        ax.set_xlabel('Time (ms)')
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.set_xlim(-1, 20)

    c = 0
    peak_nopen = []
    peak_flux = []
    average_flux = []
    peak_Ca_SS = []
    average_Ca_SS = []
    for i in range(n0, n1):
        fn = os.path.join(path, str(N_tot), '%05d.h5'%i)
        sim = SimReader(fn)

        J_release = -sim['J_efflux']
        average_flux.append(J_release)
        average_Ca_SS.append(sim['Ca_SS'])

        if sim['N_open'].max() < lim:
            continue

        if J_release.argmax() > peak_time:
            continue
        c += 1

        peak_flux.append(J_release)
        peak_Ca_SS.append(sim['Ca_SS'])
        peak_nopen.append(sim['N_open'])

    print('FRACTION OF SIMULATIONS USED FOR PEAK FLUX:', c/(n1-n0))

    peak_mean = np.mean(peak_flux, axis=0)
    peak_std = np.std(peak_flux, axis=0)

    average_mean = np.mean(average_flux, axis=0)
    average_std = np.std(average_flux, axis=0)

    peak_Ca_SS_mean = np.mean(peak_Ca_SS, axis=0)
    average_Ca_SS_mean = np.mean(average_Ca_SS, axis=0)
    peak_Ca_SS_std = np.std(peak_Ca_SS, axis=0)
    average_Ca_SS_std = np.std(average_Ca_SS, axis=0)

    ax1.plot(sim['time'], 0.001*peak_mean, color='r', lw=2, label='Peak flux')
    ax1.plot(sim['time'], 0.001*average_mean, color='b', lw=2, label='Average flux')
    ax1.legend(frameon=False, loc='upper right')

    ax2.plot(sim['time'], peak_Ca_SS_mean, color='r', label='Peak')
    ax2.plot(sim['time'], average_Ca_SS_mean, color='b', label='Mean')
    ax2.legend(frameon=False, loc='upper right')

    ax3.plot(sim['time'], np.mean(peak_nopen, axis=0), color='r', label='Peak opening')
    ax3.legend(frameon=False, loc='upper right')

    fig.savefig(os.path.join(path, f'ryr_flux_nRyR-{N_tot}.pdf'))
    plt.show()

    h5 = h5py.File(os.path.join(path, 'lee_%i.h5'%N_tot), 'w')
    h5.create_dataset('time', data=sim['time'])
    h5.create_dataset('flux peak', data=peak_mean)
    h5.create_dataset('flux peak std', data=peak_std)
    h5.create_dataset('flux mean', data=average_mean)
    h5.create_dataset('flux mean std', data=average_std)
    h5.create_dataset('Ca_SS peak', data=peak_Ca_SS_mean)
    h5.create_dataset('Ca_SS peak std', data=peak_Ca_SS_std)
    h5.create_dataset('Ca_SS mean', data=average_Ca_SS_mean)
    h5.create_dataset('Ca_SS mean std', data=average_Ca_SS_std)
    h5.close()


if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description='RyR sticky cluster model for calculating RyR flux')
    parser.add_argument('-n', '--N_tot', type=int, help='Number of RyR in a cluster')
    parser.add_argument('-a', '--average', action='store_true', help='Use this option to calculate average RyR Ca2+ flux using already saved simulations. This is also an input for FEM model.')
    parser.add_argument('-s', '--simulate', action='store_true', help='Run simulations')
    parser.add_argument('--path', default=r'./ryr_flux_simulations_1.3tau_efflux', help='Simulation path')
    args = parser.parse_args()

    # Simulations
    if args.simulate:
        out_path = os.path.join(args.path, str(args.N_tot))
        if not os.path.exists(out_path):
            os.makedirs(out_path)

        n0, n1 = 0, 100
        for i in range(n0, n1):
            dtype = h5py.string_dtype(encoding='ascii')
            voi, states, states_labels, algebraic_t, alg_labels = model(args.N_tot)

            fn = os.path.join(out_path, '%05d.h5'%i)
            print(fn)
            h5 = h5py.File(fn, 'w')
            h5.create_dataset('time', data=voi)
            dset = h5.create_dataset('states', data=states)
            h5.create_dataset('states_labels', data=str(states_labels), dtype=dtype)
            dset.attrs['labels'] = str(states_labels)
            dset = h5.create_dataset('algebraic_t', data=algebraic_t)
            h5.create_dataset('alg_labels', data=str(alg_labels), dtype=dtype)
            dset.attrs['labels'] = str(alg_labels)
            h5.close()

    # Averaging
    if args.average:
        average_flux(args.N_tot, args.path)
