# !/usr/bin/env python3
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2019-2022
#  Institute for Experimental Medical Research,
#  Oslo University Hospital and University of Oslo, Oslo, Norway
#  Authors: Yufeng Hou and Martin Laasmaa
#  This file is part of project: Sparks-Simulator
#

import numpy as np
from scipy.signal import convolve
import pylab as plt


def generate_kernel(D, dt, roi=(93, 93)):
    ft0 = 1
    y, x = np.mgrid[0:roi[0], 0:roi[1]]
    y -= roi[0]//2
    x -= roi[1]//2

    exponent = -(x**2 + y**2)/(4*D/2*dt)
    kernel = ft0/(4*np.pi*D/2*dt) * np.exp(exponent)
    ksum = kernel.sum()
    if abs(ksum - 1) > 1.e-2:
        raise ValueError('Convolution kernel panic, sum of kernel should be ONE, but is', ksum)

    return kernel


def diffusion_calc(img, kernel=None, D=42, t=0.002, pad=47):
    kernel = generate_kernel(D, t) if kernel is None else kernel
    imgpad = np.pad(img, pad, mode='reflect')
    out = convolve(imgpad, kernel, mode='same')
    out = out[pad:-pad, pad:-pad]
    return out


def t1_subtraction(img, tl=1, D=42, t=0.002, offset=0):
    out = np.empty(img.shape)
    kernel = generate_kernel(D, t)

    for i in range(len(img)):
        if i <= tl:
            out[i] = img[i]

        else:
            a1 = diffusion_calc(img[i-tl], kernel, D, t*tl)
            out[i] = img[i] - a1
            if 0:
                fig = plt.figure()
                ax1 = fig.add_subplot(131)
                ax2 = fig.add_subplot(132)
                ax3 = fig.add_subplot(133)
                vn = min(img[i].min(), a1[i].min(), out[i].min())
                vx = max(img[i].max(), a1[i].max(), out[i].max())
                ax1.imshow(img[i])
                ax2.imshow(a1)
                ax3.imshow(out[i], vmin=vn, vmax=vx)
                print(img[i].min(), img[i].max(), a1.min(), a1.max())
                plt.show()

    out = out.astype(np.float32)
    return out[tl+1:]
