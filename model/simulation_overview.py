# !/usr/bin/env python3
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2019-2020
#  Institute for Experimental Medical Research,
#  Oslo University Hospital and University of Oslo, Oslo, Norway
#  Authors: Yufeng Hou and Martin Laasmaa
#  This file is part of project: Sparks-Simulator
#

import os
import numpy as np
import h5py
import pylab as plt

from scipy.ndimage.measurements import maximum_position
from scipy.ndimage import gaussian_filter1d
from scipy.optimize import curve_fit
from scipy.integrate import simps

from utils import test_fitting #fit_2d_gaussian
from simulation_data_reader import SimulationConverter as SC
from diffusion_calculation import t1_subtraction
from model import calculate_free_buffer
import global_parameters as GP
import model_parameters_ml2 as MP

from plot_style import *


unit_mapper = {'F_Ca': '$\Delta$F/F$_0$',
               'Ca': '$\mu$M',}
label_mapper = {'F_Ca': 'FCa',
                'Ca': '[Ca$^{2+}$]$_i$',}


def autocorr(x):
    result = np.correlate(x, x, mode='full')
    return result[np.argmax(result):]


def plot_conf2():
    lw = 1.5
    fs = 16
    mpad = 8
    mpad_x = 5
    allwidth = 1.5
    d = {
        'axes.linewidth': allwidth,
        'ytick.direction': 'in',
        'xtick.direction': 'in',
        'text.usetex': True,
        'text.latex.preamble': [r"\usepackage{wasysym}",
                                r"\usepackage{upgreek}",
                                r"\everymath{\sf}",
                                r"\renewcommand{\rmdefault}{\sfdefault}"],
        'text.latex.preview': False,
        'font.size': fs,
        'font.weight': 'normal',
        'font.family': 'sans-serif',
        'font.sans-serif': ['Latin Modern Sans'],
        'xtick.labelsize': 'medium',
        'xtick.major.pad': mpad_x,
        'xtick.major.size': 7,
        'xtick.major.width': allwidth,
        'xtick.minor.pad': mpad,
        'xtick.minor.size': 5,
        'xtick.minor.width': allwidth,
        'ytick.labelsize': 'medium',
        'ytick.major.pad': mpad,
        'ytick.major.size': 7,
        'ytick.major.width': allwidth,
        'ytick.minor.pad': mpad,
        'ytick.minor.size': 5,
        'ytick.minor.width': allwidth,}
    return d


def interpolate(x, y):
    nx = np.linspace(x[0], x[-1], 100*len(x))
    ny = np.interp(nx, x, y)
    ymx, ymn = ny.max(), ny.min()
    yamp = ymx - ymn
    return nx, ny, ymx, ymn, yamp


def calc_fwhm(x, y):
    ''' returns: fwhm, x0, x1, y0, y1  '''
    nx, ny, ymx, ymn, yamp = interpolate(x, y)
    j0, j1 = np.where(ny>=0.5*yamp+ymn)[0][[0, -1]]
    return nx[j1]-nx[j0], nx[j0], nx[j1], ny[j0], ny[j1]


def calc_ttp(x, y):
    ''' returns: ttp, x0, x1, y0, y1 '''
    nx, ny, ymx, ymn, yamp = interpolate(x, y)
    j0, j1 = np.where(ny>ymn+0.01*yamp)[0][0], np.argmax(ny)
    return nx[j1]-nx[j0], nx[j0], nx[j1], ny[j0], ny[j1]


def get_plane(_data, voxel, distance=0, angle=0):
    '''
    voxel in microns
    distance in microns
    angle in degrees
    '''
    dz, dy, dx = voxel
    if _data.ndim == 3:
        data = _data
        offset = int(np.round(distance/dy, 0))
    elif _data.ndim == 4:
        k = int(np.round(distance*np.sin(angle/180*np.pi)/dz, 0))
        offset = int(np.round(distance*np.cos(angle/180*np.pi)/dy, 0))
        _, zmx, _, _ = maximum_position(_data)
        data = _data[:,zmx+k,:,:]
    return data, offset


def plot_linescan(_data, attrs, solution_type, ax1, ax2, ax3, distance=0, angle=0, ds=False, show=True):

    def convert_ff0(ax, ax_f0, f0, axis):
        """
        Update second axis according with first axis.
        """
        if axis == 'x':
            ax_f0.set_ylim(y/f0 for y in ax.get_ylim())
            ax_f0.set_ylabel('F/F$_0$')
        elif axis == 'y':
            ax_f0.set_xlim(y/f0 for y in ax.get_xlim())
            ax_f0.set_xlabel('F/F$_0$')
        ax_f0.figure.canvas.draw()

    def add_fwhm(ax, x, y, color='k'):
        fwhm, x0, x1, y0, y1 = calc_fwhm(x, y)
        #ax.plot([x0, x1], [y0,y1], color=color, ls=':')
        #ax.text(0.5*(x0+x1), y1, '%.2f'%fwhm,
        #        color=color, ha='center', va='bottom')
        return fwhm

    def add_ttp(ax, x, y, color='k'):
        ttp, x0, x1, y0, y1 = calc_ttp(x, y)
        #ax.plot([x0, x1], [y1,y1], color=color, ls=':')
        #ax.text(0.5*(x0+x1), y1, '%.2f'%ttp,
        #        color=color, ha='right', va='top')
        return ttp

    dt = attrs.get('integration step') or attrs.get('integration step [ms]')
    dz, dy, dx = attrs['element_size_um']
    data, offset = get_plane(_data, (dz, dy, dx), distance=distance, angle=angle)
    tmx, ymx, xmx = maximum_position(data)
    ymx += offset

    if solution_type == 'F_Ca':
        f0 = calculate_free_buffer(MP.C_Ca_rest, MP.k_F_on, MP.k_F_off, MP.C_DYE_tot)
    # elif solution_type == 'Ca':
    #     f0 = GP.C_Ca_rest

    # if ds:
    #     data = t1_subtraction(data, tl=1, D=1000*2*GP.D_FCa/dx/dy) + f0

    # if solution_type == 'F_Ca':
    #     data = data/f0-1# data*GP.K_FCa/(GP.C_DYE_tot - data) #+ GP.C_Ca_rest

    # elif solution_type == 'Ca':
    #     f0 = GP.C_Ca_rest
    #     #data = data*GP.C_DYE_tot/(data + GP.K_FCa)
    #     #f0 = f0*GP.C_DYE_tot/(f0 + GP.K_FCa)
    # else:
    #     raise NotImplementedError(solution_type)

    linescan = np.zeros((data.shape[0], data.shape[1]))
    for i in range(data.shape[0]):
        line = data[i,ymx,:]
        if solution_type == 'Ca':
            line = gaussian_filter1d(line, sigma=GP.FWHM_TIRF_63X[-1]/dx, mode='nearest')
        linescan[i,:] = line

    linescan = linescan/f0 - 1
    # TODO Use for analysis running average in space like 5 lines

    # FDHM
    t = dt*np.arange(data.shape[0])
    ls_dur = linescan[:, xmx]
    #ls_dur_mean = linescan[:, xmx-2:xmx+3].mean(axis=1)
    print('MIN, MAX', ls_dur.min(), ls_dur.max(), 'integral', simps(ls_dur, x=t))
    ax2.plot(t, ls_dur, color='C0')
    # ax2.plot(t, ls_dur_mean, color='C1')
    ax2.set_xlabel('Time, ms')
    ax2.set_ylabel('%s, %s' % (label_mapper[solution_type], unit_mapper[solution_type]))
    # fdhm = add_fwhm(ax2, t, ls_dur_mean, color='C1')
    # ttp = add_ttp(ax2, t, ls_dur_mean, color='C1')

    fdhm = add_fwhm(ax2, t, ls_dur, color='C0')
    ttp = add_ttp(ax2, t, ls_dur, color='C0')
    # ax2_f0 = ax2.twinx()
    # convert_ff0(ax2, ax2_f0, 1/f0, 'x')
    # ax2_f0.grid()

    # FWHM
    x = dx*np.arange(data.shape[2])
    x -= 0.5*(x[-1]-x[0])
    ls_tmx = np.argmax(ls_dur)
    ls_wid = linescan[ls_tmx]
    #ls_wid_mean = linescan[ls_tmx-2:ls_tmx+3].mean(axis=0)
    ax3.plot(x, ls_wid, color='C0')
    # ax3.plot(x, ls_wid_mean, color='C1')

    ax3.set_xlabel('x, $\mu$m')
    ax3.set_ylabel('%s, %s' % (label_mapper[solution_type], unit_mapper[solution_type]))

    #add_fwhm(ax3, x, ls_wid)
    #fwhm = add_fwhm(ax3, x, ls_wid_mean, color='C1')
    fwhm = add_fwhm(ax3, x, ls_wid, color='C0')
    #fwhm = add_fwhm(ax3, x, ls_wid_mean2, color='C2')

    # ax3_f0 = ax3.twinx()
    # convert_ff0(ax3, ax3_f0, f0, 'x')

    # Spark linescan
    spark_linescan = linescan.T#/f0
    f_amp = spark_linescan.max()
    spark = ax1.imshow(spark_linescan, interpolation='nearest', aspect='auto')
    #---- ax1.axvline(ls_tmx, lw=0.5, ls='--', color='k')
    #---- ax1.axhline(x[-1]/dx, lw=0.5, ls='--', color='k')
    ax1.set_xlim([l/dt for l in ax2.get_xlim()])
    ax1.set_xticks([])
    yticks = ax3.get_xticks()[1:-1]
    ax1.set_yticks([_x/dx+0.5*data.shape[2] for _x in yticks])
    ax1.set_yticklabels(yticks)
    ax1.set_ylabel('x, $\mu$m')
    ax1.set_title('z = %.2f $\mu$m'%distance, loc='left')
    #fig.colorbar(spark, ax=ax1, anchor=(0.0, 0.0))

    #for ax in [ax1, ax2, ax2_f0, ax3, ax3_f0]:
    for ax in [ax1, ax2, ax3]:
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)

    for ax in [ax1]:
        ax.spines['bottom'].set_visible(False)

    if show:
        plt.show()

    return f_amp, ttp, fdhm, fwhm#, fwhm_ac, fdhm_ac


def get_hm(_data, attrs, solution_type, offsets, angle=0, ds=False):

    def gauss(x, a, s):
        return a*np.exp(-x**2/2/s**2)

    # TODO this should be done so only one offset and angle is allowed
    dt = attrs.get('integration step') or attrs.get('integration step [ms]')
    dz, dy, dx = attrs['element_size_um']

    distance = 0
    if _data.ndim == 3:
        data = _data
        offset = int(np.round(distance/dy, 0))
    elif _data.ndim == 4:
        # distance is in microns
        k = int(np.round(distance*np.sin(angle/180*np.pi)/dz, 0))
        offset = int(np.round(distance*np.cos(angle/180*np.pi)/dy, 0))
        _, zmx, _, _ = maximum_position(_data)
        data = _data[:,zmx+k,:,:]
        print(angle, k, offset)

    f0 = calculate_free_buffer(MP.C_Ca_rest, MP.k_F_on, MP.k_F_off, MP.C_DYE_tot)
    if ds:
        data = t1_subtraction(data-f0, tl=1, D=1000*2*GP.D_FCa/dx/dy) + f0

    tmx, ymx_, xmx = maximum_position(data)

    fwmxs = []
    fdmxs = []
    ttps = []
    amps = []
    for offset in offsets:
        ymx = ymx_ + offset
        linescan = np.zeros((data.shape[0], data.shape[1]))
        for i in range(data.shape[0]):
            linescan[i,:] = data[i,ymx,:]

        linescan = linescan/f0
        ls_dur = linescan[:, xmx]
        x = dx*np.arange(data.shape[2])
        x -= 0.5*(x[-1]-x[0])
        ls_tmx = np.argmax(ls_dur)
        ls_wid_mean = linescan[ls_tmx]

        print(f0, ls_wid_mean.max())
        #ls_wid_mean = linescan[ls_tmx-2:ls_tmx+3].mean(axis=0)
        fwhm, x0, x1, y0, y1 = calc_fwhm(x, ls_wid_mean)
        fwmxs.append(fwhm)
        amps.append(ls_wid_mean.max())
        #amps.append(data.max()/f0)

        t = dt*np.arange(data.shape[0])
        ttps.append(calc_ttp(t, ls_dur)[0])
        fdmxs.append(calc_fwhm(t, ls_dur)[0])

        mx = np.argmax(ls_wid_mean)
        half_fwhm = ls_wid_mean[mx:]*f0
        half_x = x[mx:]

        p0 = [half_fwhm.max(), 0.2]
        popt, pcov = curve_fit(gauss, half_x, half_fwhm-f0, p0=p0)

        v1 = simps(half_fwhm-f0, half_x)
        v0 = simps(half_x**2*f0, half_x)

    return np.array(fwmxs), np.array(amps), np.array(fdmxs), np.array(ttps)


def timeseries_2d(args):
    path = args.simulation_file[0]
    plt.rcParams.update(plot_conf())
    fig = plt.figure(figsize=(15,10))
    fig.subplots_adjust(left=0.07, bottom=0.12, right=0.99, top=0.95,
                        wspace=0.3, hspace=0.3)
    # ax1 = fig.add_subplot(241)
    # ax2 = fig.add_subplot(242)
    # ax3 = fig.add_subplot(243)
    # ax4 = fig.add_subplot(244)

    data, attrs = SC.load_data(path, args.solution_type)
    dt = attrs.get('integration step') or attrs.get('integration step [ms]')
    dz, dy, dx = attrs['element_size_um']

    f0 = calculate_free_buffer(MP.C_Ca_rest, MP.k_F_on, MP.k_F_off, MP.C_DYE_tot)
    im, _ = get_plane(data, (dz, dy, dx), distance=0, angle=0)
    #print(im.max(), im.min())
    # im = im# + np.random.normal(0, 0.8, im.shape)
    im = im / f0 - 1

    #foo = test_fitting(im[int(6/dt)], f0)

    times = [6, 8, 10, 12]
    for i, t in enumerate(times):
        n = int(t/dt)
        ax = fig.add_subplot(3,4,1+i)
        if i==0:
            vmin, vmax = 0, 1.7#im[n].min(), im[n].max()
        ax.imshow(im[n], vmin=vmin, vmax=vmax, interpolation='nearest')
        ax.set_xticks([])
        ax.set_yticks([])
        ax.spines['left'].set_visible(False)
        ax.spines['bottom'].set_visible(False)

    # line scans
    ax2 = fig.add_subplot(312)
    ax3 = fig.add_subplot(313)


    zmx, ymx, xmx = maximum_position(im)
    ls = im[:,:,xmx].T

    neg = ax2.imshow(ls, vmin=vmin, vmax=vmax, aspect='auto', interpolation='nearest')
    X, Y = np.meshgrid(np.arange(0, ls.shape[1]),
                       np.arange(0, ls.shape[0]))
    mx = ls.max()
    levels = [0.1*mx, 0.25*mx, 0.5*mx, 0.75*mx, 0.99*mx]
    ax2.contour(X, Y, ls, levels=levels, colors='w', linewidths=0.75, linestyles='dotted')

    im, _ = get_plane(data, (dz, dy, dx), distance=0.5, angle=90)
    im = im / f0 - 1
    zmx, ymx, xmx = maximum_position(im)
    ls = im[:,:,xmx].T
    peg = ax3.imshow(ls, vmin=vmin, vmax=vmax, aspect='auto', interpolation='nearest')
    mx = ls.max()
    levels = [0.1*mx, 0.25*mx, 0.5*mx, 0.75*mx, 0.99*mx]
    ax3.contour(X, Y, ls, levels=levels, colors='w', linewidths=0.75, linestyles='dotted')

    fig.colorbar(neg, ax=ax2)
    fig.colorbar(peg, ax=ax3)

    for ax in [ax2, ax3]:
        ax.set_xticks([])
        ax.set_yticks([])
        ax.spines['left'].set_visible(False)
        ax.spines['bottom'].set_visible(False)

    save_path = path[:-3]
    plt.savefig(save_path+'_timeseries.pdf')
    plt.show()


def hm_relationship(args):
    def sq(x, a, b, c, d):
        return a*x**b + d*x + c

    def mm(s, kd, vmax):
        return s*vmax/(s+kd)

    # path = r'/media/martin/Seagate Expansion Drive/spark-simulations/fem-simulations/different_release_profiles/integrated/2d-tirf'
    # path = r'/home/martin/code/sparks-simulator/model/simulated-data/lee_3d_simulations/integrated'
    path = args.simulation_file[0]
    plt.rcParams.update(plot_conf())
    fig = plt.figure(figsize=(15,7))
    fig.subplots_adjust(left=0.08, bottom=0.12, right=0.99, top=0.95,
                        wspace=0.3, hspace=0.3)
    ax2 = fig.add_subplot(241)
    ax1 = fig.add_subplot(242)
    ax3 = fig.add_subplot(243)
    ax4 = fig.add_subplot(244)
    ax5 = fig.add_subplot(212)

    solution_type = args.solution_type
    offsets = np.array([0, 1, 2, 5, 7, 10, 15, 20, 25, 30])
    jamps = np.array([100, 250, 500, 1000, 2500])
    jamps = np.array([100, 250, 500, 1000, 2500, 3000])/2
    jamps = np.array([250, 500, 750, 1000, 1250, 1500])
    jamps = np.array([2, 5, 10, 15, 20, 25, 30, 50])
    #jamps = np.array([10, 20, 50, 100])

    flux_types = ['ryr_flux', 'reverse-triangle', 'triangle', 'exponential', 'lee_peak']
    flux_type = flux_types[-1]
    nryr_amps = []

    color = '#666666'
    mfc = 'w'
    mec = 'k'
    lw = 1
    markers = ['v', 'o', 's', 'D', '^', '1', 'P']
    mi = 0
    for jamp in jamps:
        fn = f'3d_{flux_type}_J{int(jamp)}_100ms_10us_integ-dt2.0ms.h5'
        data, attrs = SC.load_data(os.path.join(path,fn), solution_type)
        dt = attrs.get('integration step') or attrs.get('integration step [ms]')
        dz, dy, dx = attrs['element_size_um']
        fwhms, amps, fdhms, ttps = get_hm(data, attrs, solution_type,
                                          offsets, angle=90, ds=args.diffusion_subtraction)
        amps -= 1

        print(fn, amps, args.diffusion_subtraction)

        if jamp in [2, 5, 10, 15, 20, 25, 30]:#, 50]:
            ax1.plot(dy*offsets, fwhms, color=color, marker=markers[mi], markerfacecolor=mfc, markeredgecolor=mec, ls=':', lw=lw, label=f'{jamp}')
            ax2.plot(dy*offsets, amps, color=color, marker=markers[mi], markerfacecolor=mfc, markeredgecolor=mec, ls=':', lw=lw, label=f'{jamp}')
            ax3.plot(dy*offsets, fdhms, color=color, marker=markers[mi], markerfacecolor=mfc, markeredgecolor=mec, ls=':', lw=lw, label=f'{jamp}')
            ax4.plot(dy*offsets, ttps, color=color, marker=markers[mi], markerfacecolor=mfc, markeredgecolor=mec, ls=':', lw=lw, label=f'{jamp}')
            mi += 1
            #ax3.plot(fwhms, amps/amps[0], marker='s', ls=':', label=f'{jamp}')
            #ax3.plot(fwhms, amps, marker='s', ls=':', label=f'{jamp}')

            tmp = fwhms/amps
            #tmp = amps/amps[0]
            # tmp -= tmp[0]
            tmp /= tmp[-1]
            #ax4.plot(dy*offsets, tmp, marker='s', ls=':', label=f'J{jamp}')
            # popt, pcov = curve_fit(sq, dy*offsets, tmp, p0=[5,2,0,0])
            # print(popt)
            # ax4.plot(dy*offsets, sq(dy*offsets, *popt), ls='--', color='r')

        nryr_amps.append([jamp, amps[0]])

    nryr_amps = np.array(nryr_amps)
    p0 = [10, 2.8]
    popt, pcov = curve_fit(mm, nryr_amps[:,0], nryr_amps[:,1], p0=p0)
    print(popt)
    nryrs = np.arange(0, jamps[-1]*1.1, 1)
    ax5.plot(nryrs, mm(nryrs, *popt), ls='--', color='#666666',#color='r',
             #label=r'Fit: $\frac{F_{max}\times nRyR}{K_d + nRyR}$; \small{$K_d=$%.1f, $F_{max}=$%.1f}'%tuple(popt)
             label=r'Fit')
    ax5.plot(nryr_amps[:,0], nryr_amps[:,1], ls='', marker='s', label='Simulations', color='#666666', markerfacecolor='w')
    #ax3.axvline(1.1, ls=':', color='k')

    #ax1.set_xlabel(r'Distance from release site ($\mu$m)')
    ax1.set_xlabel(r'z ($\mu$m)')
    ax1.set_ylabel('FWHM ($\mu$m)')

    ax2.set_xlabel(r'z ($\mu$m)')
    ax2.set_ylabel('Amplitude ($\Delta$F/F$_0$)')
    ax2.legend(frameon=False, title='nRyR')

    ax3.set_xlabel(r'z ($\mu$m)')
    ax3.set_ylabel('FDHM (ms)')

    ax4.set_xlabel(r'z ($\mu$m)')
    ax4.set_ylabel('TTP (ms)')

    #ax3.set_xlabel('FWHM, $\mu$m')
    #ax3.set_ylabel('Amplitude, $\Delta$F/F$_0$')
    #ax3.set_ylabel('Normalized amplitude')
    #ax3.set_ylabel('Normalized amplitude, $\Delta$F/F$_0$/F$_{max}$')

    #ax4.set_xlabel('Distance from release site, $\mu$m')
    #ax4.set_ylabel('FWHM/($\Delta$F/F$_0$/F$_{max}$)')

    ax5.set_xlabel('nRyR')
    ax5.set_ylabel('Amplitude ($\Delta$F/F$_0$)')
    ax5.legend(frameon=False, loc='lower right')

    for ax in [ax1, ax2, ax3, ax4, ax5]:
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)

    save_path = args.save_path
    save_path = path
    np.savetxt(os.path.join(save_path, 'exp_points-%s.csv'%flux_type), nryr_amps)

    np.savetxt(os.path.join(save_path, 'mm_points-%s.csv'%flux_type), np.array([nryrs, mm(nryrs, *popt)]).T)

    plt.savefig(os.path.join(save_path, 'out-of-foucus-%s.pdf'%flux_type))
    plt.show()


def single_plot(args):
    solution_type = args.solution_type
    data, attrs = SC.load_data(args.simulation_file[0], solution_type)
    #dt = attrs.get('integration step') or attrs.get('integration step [ms]')
    #dz, dy, dx = attrs['element_size_um']

    fig = plt.figure()
    ax1 = fig.add_subplot(311)
    ax2 = fig.add_subplot(312)
    ax3 = fig.add_subplot(313)
    #plot_linescan(data, attrs, solution_type, ax1, ax2, ax3, ds=args.diffusion_subtraction)

    solution_type = 'F_Ca'
    data, attrs = SC.load_data(args.simulation_file[0], solution_type)
    plot_linescan(data, attrs, solution_type, ax1, ax2, ax3, ds=True, show=False)
    solution_type = 'Ca'
    data, attrs = SC.load_data(args.simulation_file[0], solution_type)
    plot_linescan(data, attrs, solution_type, ax1, ax2, ax3, ds=False)


def plot(args):
    solution_type = args.solution_type
    data, attrs = SC.load_data(args.simulation_file[0], solution_type)
    dt = attrs.get('integration step') or attrs.get('integration step [ms]')
    dz, dy, dx = attrs['element_size_um']

    plt.rcParams.update(plot_conf())
    fig = plt.figure(figsize=(16,9))
    fig.subplots_adjust(left=0.05, bottom=0.08, right=0.99, top=0.95,
                        wspace=0.3, hspace=0.3)

    offsets_r = dy*np.array([0, 5, 10, 20])
    nrows = 3
    ncols = len(offsets_r)+1
    results = []
    angle = 0
    for i, offset in enumerate(offsets_r):
        ax1 = fig.add_subplot(nrows, ncols, i+1)
        ax2 = fig.add_subplot(nrows, ncols, i+1+ncols)
        ax3 = fig.add_subplot(nrows, ncols, i+1+2*ncols)
        r = plot_linescan(data, attrs, solution_type, ax1, ax2, ax3, offset, angle=angle, ds=args.diffusion_subtraction, show=False)

    offsets = dy*np.array([0, 1, 2, 5, 7, 10, 15, 20, 25, 30])
    fig2 = plt.figure()
    for i, offset in enumerate(offsets):
        ax1 = fig2.add_subplot(1, 3, 1)
        ax2 = fig2.add_subplot(1, 3, 2)
        ax3 = fig2.add_subplot(1, 3, 3)
        r = plot_linescan(data, attrs, solution_type, ax1, ax2, ax3, offset, angle=angle, ds=args.diffusion_subtraction, show=False)
        results.append(r)

    print(offsets)
    results = np.array(results)
    labels = ['Amplitude, $\Delta$F/F$_0$', 'TTP, ms', 'FDHM, ms', 'FWHM, $\mu$s']
    for j in range(results.shape[1]):
        ax = fig.add_subplot(nrows+1, ncols, ncols+j*ncols)
        ax.plot(offsets, results[:,j], marker='s', ls=':', color='C0')
        ax.scatter(offsets[[0,3,5,7]], results[:,j][[0,3,5,7]], marker='+', color='C1', zorder=10)
        print(offsets[[0,3,5,7]], results[:,j][[0,3,5,7]])
        ax.set_ylabel(labels[j])
        ax.grid()#axis='x')
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        if j < nrows:
            ax.set_xticklabels([])
    ax.set_xlabel('Distance from source, $\mu$m')

    fn = os.path.join(args.save_path, os.path.basename(args.simulation_file[0])[:-3]+'%s.pdf'%('' if args.diffusion_subtraction is False else '_-ds'))
    fig.savefig(fn, dpi=300)
    print(fn)
    plt.show()


if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(description='Summarizes results of simulation data')
    parser.add_argument('-t', '--solution_type', default='F_Ca', help='Solution type that is displayied. Possible types are F_Ca, Ca')
    parser.add_argument('-o', '--save_path', default='./results', help='Path where summarizing pdf is saved')
    parser.add_argument('-ds', '--diffusion_subtraction', action='store_true', help='Apply diffusion subtraction. Default False.')
    parser.add_argument('simulation_file', nargs=1, help='Integrated simulation file')
    args = parser.parse_args()

    #single_plot(args)
    plot(args)

    #timeseries_2d(args)
    #hm_relationship(args)
