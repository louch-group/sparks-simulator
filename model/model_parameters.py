# This is a set of parameters

D_Ca = 0.3        # calcium diffusion coefficient, um^2/ms
C_Ca_rest = 0.1   # resting cyto calcium concentration

# Dye
D_F = 0.042       # calcium unbound dye diffusion coefficient, um^2/ms
D_FCa = 0.042     # calcium bound dye diffusion coefficient, um^2/ms
k_F_on = 0.1      # calcium dye binding rate, uM^-1 * ms^-1
k_F_off = 0.11    # calcium dye off rate ms^-1
K_FCa = k_F_off/k_F_on  # dye dissociation constant, uM
C_DYE_tot = 25    # dye concentration, uM

# Calmodulin
D_CAL = 0.022     # calmodulin diffusion coefficient, um^2/ms
C_CAL_tot = 24.   # calmodulin total concentration, uM
k_CAL_on = 0.034  # calmodulin binding rate, uM^-1 * ms^-1
k_CAL_off = 0.238 # calmodulin off rate ms^-1

# Supporting material for: Control of sarcoplasmic reticulum Ca2+ release by stochastic RyR
# gating within a 3D model of the cardiac dyad and importance of induction decay for CICR
# DOI: 10.1016/j.bpj.2013.03.058
D_ATP = 0.030     # ATP diffusion coefficient, um^2/ms - from simson et al 2016
C_ATP_tot = 4000  # ATP total concentration, uM
k_ATP_on = 0.0137 # ATP binding rate, uM^-1 * ms^-1
k_ATP_off = 30.   # ATP off rate ms^-1

# Troponin
D_TRP = 0.0       # Troponin diffusion coefficient, um^2/ms
C_TRP_tot = 70.0  # Troponin total concentration, uM
k_TRP_on = 0.0327 # Troponin binding rate, uM^-1 * ms^-1
k_TRP_off = 0.0196# Troponin off rate ms^-1

# SERCA parameters <-- https://models.physiomeproject.org/exposure/59a44249dec83576d97fd3fce46ec5f9/niederer_smith_2007.cellml/cellml_math
G_SER = 0.45      # SERCA maximal conductance, uM/ms
K_SER = 0.5       # SERCA dissociation constant, uM
# calculating leak to match SERCA uptake rate at rest
JLEAK = G_SER*C_Ca_rest**2/(K_SER**2+C_Ca_rest**2)


def parameters_table(buffer_schemes, parameters_list, delimiter=','):

    with open('parameters_overview_table.csv', 'w') as f:
        f.write(delimiter.join(['Scheme'] + parameters_list)+'\n')
        for buffer_scheme in buffer_schemes:
            MP = __import__('model_parameters_' + buffer_scheme)
            f.write(delimiter.join([buffer_scheme] + [str(getattr(MP, p)) for p in parameters_list])+'\n')


if __name__ == '__main__':
    buffer_schemes = ['ml2', 'kong', 'kolstad', 'vijay', 'cannell']
    parameters_list = ['D_Ca', 'D_F', 'k_F_on', 'k_F_off', 'C_DYE_tot',
                       'D_CAL', 'k_CAL_on', 'k_CAL_off', 'C_CAL_tot',
                       'D_ATP', 'k_ATP_on', 'k_ATP_off', 'C_ATP_tot',
                       'D_TRP', 'k_TRP_on', 'k_TRP_off', 'C_TRP_tot',]
    
    parameters_table(buffer_schemes, parameters_list)
