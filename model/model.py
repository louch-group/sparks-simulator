# !/usr/bin/env python3
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2019-2022
#  Institute for Experimental Medical Research,
#  Oslo University Hospital and University of Oslo, Oslo, Norway
#  Authors: Yufeng Hou and Martin Laasmaa
#  This file is part of project: Sparks-Simulator
#


from fenics import *
from mshr import *
import numpy as np
import h5py
import os

import global_parameters as GP

from utils import cartesian_coordinates, ryr_flux, get_ryr_flux_nodes, lee_flux

# imports for testing purposes
import time
import pylab as plt


class SinglePS(object):

    def __init__(self, x0, y0, z0, t0, t1, J=10, flux_type='triangle', flux_path=None):
        self.J = J
        self.J_baseline = 25.0
        self.x0 = x0
        self.y0 = y0
        self.z0 = z0
        self.t0 = t0
        self.t1 = t1
        self.duration = t1-t0
        self.tau = 0.15*self.duration
        self.flux_type = flux_type
        self.flux_path = flux_path
        self.flux_function = None

        if self.flux_type == 'constant':
            self.flux_function = self.j_constant
        elif self.flux_type == 'triangle':
            self.flux_function = self.j_triangle
        elif self.flux_type == 'reverse-triangle':
            self.flux_function = self.j_reverse_triangle
        elif self.flux_type == 'exponential':
            self.flux_function = self.j_exponential
        elif self.flux_type == 'ryr_flux':
            # profiles from https://www.cell.com/biophysj/fulltext/S0006-3495(02)75149-7 page65 fig5
            # in this case J is the number of RyR in cluster
            _x, _y = np.array(get_ryr_flux_nodes(J))
            _x += self.t0
            self.t1 = _x.max()
            self.duration = self.t1-self.t0
            self.J = (200*_y.max() + 240)/_y.max()
            spl = ryr_flux(_x, _y)
            self.flux_function = lambda t: self.J*spl(t)

        elif self.flux_type == 'lee_peak' or self.flux_type == 'lee_mean':
            # Lee et al 2012, http://dx.doi.org/10.1016/j.bpj.2012.12.055
            spl = lee_flux(J, t0=self.t0, flux_type=self.flux_type, path=self.flux_path)
            c = GP.V_SS/GP.V_ME
            self.t1 = 100
            self.flux_function = lambda t: c*spl(t)

    def j_constant(self, t):
        return self.J

    def j_triangle(self, t):
        #https://www.cell.com/biophysj/fulltext/S0006-3495(02)75149-7 page-74
        return self.J*(1-(t-self.t0)/self.duration)

    def j_reverse_triangle(self, t):
        #https://www.cell.com/biophysj/fulltext/S0006-3495(02)75149-7 page-74
        return -self.J*(self.t0-t)/self.duration

    def j_exponential(self, t):
        #https://www.cell.com/biophysj/fulltext/S0006-3495(02)75149-7 page-74
        # return 0.97*self.J*np.exp((self.t0-t)/self.tau) + 0.03*self.J
        return np.abs((self.J-self.J_baseline))*np.exp((self.t0-t)/self.tau)+self.J_baseline

    def __call__(self, x, t):
        xx = (x[0] - self.x0)
        yy = (x[1] - self.y0)
        zz = 0#(x[2] - self.z0)

        if self.t0 <= t <= self.t1:
            if pow(xx,2) + pow(yy,2) + pow(zz,2) < GP.PS_RADIUS*GP.PS_RADIUS:
                return self.flux_function(t)

        return 0.0

    def calc_j(self, t):

        if self.t0 <= t <= self.t1:
            return self.flux_function(t)

        return 0.0


class MultiPS(UserExpression):

    def __init__(self, points, flux_type='triangle', flux_path=None, **kwargs):
        '''
        points array of [posx, posy, posz, t0, t1, J]
        '''
        UserExpression.__init__(self, **kwargs)
        self.points = points
        self.t = 0.0
        self.sources = [SinglePS(*pnt, flux_type=flux_type, flux_path=flux_path) for pnt in self.points]

    def eval(self, values, x):
        v = 0.0
        for ps in self.sources:
            v += ps(x, self.t)
        values[0] = v

    def value_shape(self):
        return []


class FEModel_small(object):

    from_mp = ['D_Ca', 'D_F', 'k_F_on', 'k_F_off', 'C_DYE_tot',
               'D_CAL', 'k_CAL_on', 'k_CAL_off', 'C_CAL_tot',
               'D_ATP', 'k_ATP_on', 'k_ATP_off', 'C_ATP_tot',
               'D_TRP', 'k_TRP_on', 'k_TRP_off', 'C_TRP_tot',
               'JLEAK', 'K_SER', 'G_SER']

    @staticmethod
    def boundary(x, on_boundary):
        return on_boundary

    def __init__(self, FS_args, dt, t0, t1, u_0, px, py, pz,
                 xbounds, ybounds, zbounds, concentration_labels,
                 point_sources=None,
                 save_path=None, save_vtk=None):
        '''
        time is in milliseconds
        concentrations are in micromolar
        '''
        self.function_space_args = FS_args

        self.V = FunctionSpace(*self.function_space_args)
        self.dt = dt
        self.t0 = t0
        self.t1 = t1
        self.px = px
        self.py = py
        self.pz = pz
        self.xbounds = xbounds
        self.ybounds = ybounds
        self.zbounds = zbounds

        self.point_sources = point_sources

        self.k = dt
        for key in self.from_mp:
            setattr(self, key, Constant(getattr(MP, key)))
        self.K_SER = Constant(self.K_SER*self.K_SER)

        self.save = True if save_path is not None else False
        self.save_path = save_path
        self.results_file = None
        self.coordinates = cartesian_coordinates(xbounds, ybounds, zbounds, px, py, pz)
        self.result_array_shape = [el.size for el in self.coordinates]

        self.concentration_labels = concentration_labels
        self.dim = len(self.concentration_labels)

        self.vtkfile = None
        if save_vtk is not None:
            self.vtkfile = [File(os.path.join(save_vtk, 'u{}_{}.pvd'.format(i, v))) for i, v in enumerate(self.concentration_labels)]
        self.save_vtk = True if save_vtk is not None else False

        # Define variational problem
        u = self.u = [TrialFunction(self.V) for i in range(self.dim)]
        v = self.v = [TestFunction(self.V) for i in range(self.dim)]
        u_ = self.u_ = [Function(self.V) for i in range(self.dim)]
        self.u_0 = u_0
        u_n = self.u_n = [interpolate(_u_0, self.V) for _u_0 in self.u_0]

        # boundary conditions
        self.boundary_cs = [DirichletBC(self.V, i, self.boundary) for i in self.u_0]

        if self.point_sources is None:
            self.f_1 = Constant(0.0)
        else:
            self.f_1 = self.point_sources

        self.Jser = Expression('(u < Ca0) ? 0 : g*pow(u,2)/(k+pow(u,2))-l',
                               degree=1, u=u_n[0], Ca0=1.00001*MP.C_Ca_rest,
                               g=MP.G_SER, k=MP.K_SER**2, l=self.JLEAK)

        self.Jdye = self.k_F_on*u_n[0]*(self.C_DYE_tot - u_n[1]) - self.k_F_off*u_n[1]
        self.Jcal = self.k_CAL_on*u_n[0]*(self.C_CAL_tot - u_n[2]) - self.k_CAL_off*u_n[2]
        self.Jatp = self.k_ATP_on*u_n[0]*(self.C_ATP_tot - u_n[3]) - self.k_ATP_off*u_n[3]
        self.Jtrp = self.k_TRP_on*u_n[0]*(self.C_TRP_tot - u_n[4]) - self.k_TRP_off*u_n[4]

        a_0 = u[0]*v[0]*dx + self.D_Ca*self.k*inner(grad(u[0]), grad(v[0]))*dx
        L_0 = (u_n[0] + self.k*(self.f_1 - self.Jdye - self.Jser - self.Jcal - self.Jatp - self.Jtrp))*v[0]*dx
        #L_0 = (u_n[0] + self.k*self.f_1)*v[0]*dx

        a_1 = u[1]*v[1]*dx + self.D_F*self.k*inner(grad(u[1]), grad(v[1]))*dx
        L_1 = (u_n[1] + self.k*self.Jdye)*v[1]*dx

        a_2 = u[2]*v[2]*dx + self.D_CAL*self.k*inner(grad(u[2]), grad(v[2]))*dx
        L_2 = (u_n[2] + self.k*self.Jcal)*v[2]*dx

        a_3 = u[3]*v[3]*dx + self.D_ATP*self.k*inner(grad(u[3]), grad(v[3]))*dx
        L_3 = (u_n[3] + self.k*self.Jatp)*v[3]*dx

        a_4 = u[4]*v[4]*dx # Troponin is not mobile
        L_4 = (u_n[4] + self.k*self.Jtrp)*v[4]*dx

        self.L = [L_0, L_1, L_2, L_3, L_4]
        self.A = [assemble(a_0), assemble(a_1), assemble(a_2), assemble(a_3), assemble(a_4)]
        # Apply boundary conditions to matrices
        [bc.apply(A) for bc, A in zip(self.boundary_cs, self.A)]

    def solve(self):
        if self.save:
            self.open_hdf5()
            self.save_result(self.u_n, 0)

        # self.plot_result(self.u_n)
        num_steps = int(round((self.t1-self.t0)/self.dt))
        t = self.t0
        for n in range(num_steps):
            # Update current time
            t += self.k
            if self.point_sources is not None:
                self.f_1.t = t

            if n%100 == 0:
                print(t)
            # Compute solution
            for i in range(self.dim):
                b = assemble(self.L[i])
                self.boundary_cs[i].apply(b)

                # optimal parameters for solver in following book (PAGE 371):
                # https://books.google.no/books?id=WbqdDQAAQBAJ&pg=PA371&lpg=PA371&dq=%27hypre_amg%27&source=bl&ots=rhfOvjXeVj&sig=ACfU3U2MNy1YRD516PquQLRnNU2UNSjXxg&hl=en&sa=X&ved=2ahUKEwjInt-Mp6bnAhXpiIsKHUjMDMUQ6AEwCHoECAoQAQ#v=onepage&q='hypre_amg'&f=false
                solve(self.A[i], self.u_[i].vector(), b, 'cg', 'sor')
                self.u_n[i].assign(self.u_[i])

                if self.save_vtk:
                    self.vtkfile[i] << (self.u_[i], t)

            if self.save:
                self.save_result(self.u_, n+1)

        self.close_hdf5()

    def save_result(self, u, n_step):
        '''Saves intermedinate results to hdf5 file'''
        for i, _u in enumerate(u):
            self.results_file.write(_u, '/u{}/values_{:08d}'.format(i,n_step))

    def open_hdf5(self):
        '''Prepares HDF5 file for saving intermedinate results'''
        m = Mesh()
        self.results_file = HDF5File(m.mpi_comm(), self.save_path, 'w')
        self.results_file.write(self.function_space_args[0], '/mesh')

        self.results_file.write(Function(self.V), 'Configuration')
        attrs = self.results_file.attributes('Configuration')

        for key in ['t0','t1','dt','px','py','pz']+self.from_mp:
            val = getattr(self, key)
            if isinstance(val, Constant):
                val = val.values()[0]
            attrs[key] = val

        attrs['solution labels'] = repr(self.concentration_labels)
        attrs['space xbounds'] = repr(self.xbounds)
        attrs['space ybounds'] = repr(self.ybounds)
        attrs['space zbounds'] = repr(self.zbounds)
        attrs['mesh_family'] = repr(self.function_space_args[1])
        attrs['mesh_degree'] = repr(self.function_space_args[2])

    def close_hdf5(self):
        if self.results_file is not None:
            self.results_file.close()

    def plot_result(self, u):
        res = np.zeros((self.dim, *self.result_array_shape[:-1]))
        for i, x in enumerate(self.coordinates[0]):
            for j, y in enumerate(self.coordinates[1]):
                for k in range(self.dim):
                    res[k,i,j] = u[k](x,y)

        fig = plt.figure()
        print('Mean values of:')
        mx = None
        for i, conc in enumerate(self.concentration_labels):
            d = res[i]
            print('  ', conc, d.mean())

            ax = fig.add_subplot(2, self.dim, i+1)
            ax.set_title(conc)
            ax_ = fig.add_subplot(2, self.dim, self.dim+i+1)

            ax.imshow(d, origin='lower')
            if mx is None:
                mx = np.where(d==d.max())

            line = d[:,mx[1]]
            ax_.plot(line)
            ax_.axhline(0.5*(line.min()+line.max()), color='k')

        plt.show()


def calculate_free_buffer(c, on, off, tot):
    return on*c*tot/(on*c+off)


def calculate_ps_volume(ps, FS_args):
    volume_before0 = assemble(ps*dx(domain=FS_args[0]))*1.e-15*1.e6
    print('calculate_ps_volume', volume_before0)
    return volume_before0


def small(jamp=100, flux_type='exponential', flux_path=None, buffering_type=None, mesh_filename=None):
    # Form compiler options
    parameters["form_compiler"]["optimize"]     = True
    parameters["form_compiler"]["cpp_optimize"] = True
    parameters["form_compiler"]["representation"] = "uflacs"

    from generate_mesh import get_points, RyRMesher

    points = get_points()
    xbounds = 0, 6
    ybounds = 0, 6
    zbounds = 0, 5

    mesh = Mesh()
    mesh_file = HDF5File(mesh.mpi_comm(), mesh_filename, 'r')
    mesh_file.read(mesh, mesh_filename.split('/')[-1], False)

    FS_args = mesh, 'CG', 2

    if 0:
        test_ps = MultiPS([[3,3,0,0,1,1]], flux_type='constant')
        vol = calculate_ps_volume(test_ps, FS_args)
        print('conversion factor', MP.V_SS/vol, MP.V_SS/(vol*4/3))

    if 1: # single release
        x, y, z = 3, 3, 0,
        t0 = 6
        t1 = 30
        J = jamp

    ps = MultiPS([[x,y,z,t0,t1,J]], flux_type=flux_type, flux_path=flux_path)

    if 1:
        concentration_labels = ['Ca', 'F_Ca', 'CAL_Ca', 'ATP_Ca', 'TRP_Ca']
        F_Ca = calculate_free_buffer(MP.C_Ca_rest, MP.k_F_on, MP.k_F_off, MP.C_DYE_tot)
        CAL_Ca = calculate_free_buffer(MP.C_Ca_rest, MP.k_CAL_on, MP.k_CAL_off, MP.C_CAL_tot)
        ATP_Ca = calculate_free_buffer(MP.C_Ca_rest, MP.k_ATP_on, MP.k_ATP_off, MP.C_ATP_tot)
        TRP_Ca = calculate_free_buffer(MP.C_Ca_rest, MP.k_TRP_on, MP.k_TRP_off, MP.C_TRP_tot)

        u_0 = [
            Expression('X', degree=1, X=MP.C_Ca_rest),
            Expression('X', degree=1, X=F_Ca),
            Expression('X', degree=1, X=CAL_Ca),
            Expression('X', degree=1, X=ATP_Ca),
            Expression('X', degree=1, X=TRP_Ca),
            ]

    result_file = '%s_J%i_100ms_10us' % (flux_type, jamp)
    results_path = './simulated-data/%s' % buffering_type
    save_vtk = None
    # save_vtk = './simulated-data/vtks/%s/%s/' % (buffering_type, result_file)

    model = FEModel_small(FS_args, dt=0.01, t0=0.0, t1=100.0, u_0=u_0,
                          px=0.05, py=0.05, pz=0.1,
                          xbounds=xbounds, ybounds=ybounds, zbounds=zbounds,
                          concentration_labels=concentration_labels,
                          point_sources=ps,
                          save_path=os.path.join(results_path, result_file+'.h5'),
                          save_vtk=save_vtk)

    time0 = time.time()
    model.solve()
    print('Took', time.time()-time0, 's')
    print('Files saved:', model.save_path, model.save_vtk)


if __name__ == '__main__':
    # run as: mpiexec -n 2 python3 model.py
    import argparse
    parser = argparse.ArgumentParser(description='FEM model for calculating flux spatio-temporal evolution')
    parser.add_argument('-ft', '--flux-type', type=str, help='Specify flux type. For example: `lee_mean` or `lee_peak`')
    args = parser.parse_args()
    ftype = args.flux_type

    for buffering_type in ['ml2', 'kolstad', 'kong', 'vijay']:
        for ftype in ['lee_peak', 'lee_mean']:
        #if 1:
            MP = __import__('model_parameters_'+buffering_type)
            for jamp in [2, 5, 10, 15, 25, 30, 50]:
                print(f'RyRs {jamp} and flux type {ftype}')
                small(jamp, flux_type=ftype, flux_path='./ryr_flux_simulations_for_article/',
                      buffering_type=buffering_type,
                      mesh_filename='./test_data/small_test_mesh_2D_grade32.h5')
