# Model parameters from Kolstad et al 2018
# https://elifesciences.org/articles/39427/

D_Ca = 0.22       # calcium diffusion coefficient, um^2/ms
C_Ca_rest = 0.1   # resting cyto calcium concentration

# Dye
D_F = 0.042       # calcium unbound dye diffusion coefficient, um^2/ms
D_FCa = 0.042     # calcium bound dye diffusion coefficient, um^2/ms
k_F_on = 0.1      # calcium dye binding rate, uM^-1 * ms^-1 
k_F_off = 0.11    # calcium dye off rate ms^-1 actual paper has a typo in kon and koff this is fixed here accorind to the source Picht et al 2011 https://www.ahajournals.org/doi/suppl/10.1161/CIRCRESAHA.111.240234
K_FCa = k_F_off/k_F_on # dye dissociation constant, uM
C_DYE_tot = 25    # dye concentration, uM

# Calmodulin
D_CAL = 0.022     # calmodulin diffusion coefficient, um^2/ms
C_CAL_tot = 24.   # calmodulin total concentration, uM
k_CAL_on = 0.034  # calmodulin binding rate, uM^-1 * ms^-1
k_CAL_off = 0.238 # calmodulin off rate ms^-1

# ATP
D_ATP = 0.140     # ATP diffusion coefficient, um^2/ms
C_ATP_tot = 455   # ATP total concentration, uM
k_ATP_on = 0.255  # ATP binding rate, uM^-1 * ms^-1
k_ATP_off = 45.     # ATP off rate ms^-1

# Troponin
D_TRP = 0.0       # Troponin diffusion coefficient, um^2/ms
C_TRP_tot = 70.0  # Troponin total concentration, uM
k_TRP_on = 0.0327 # Troponin binding rate, uM^-1 * ms^-1
k_TRP_off = 0.0196# Troponin off rate ms^-1

# SERCA parameters <-- https://models.physiomeproject.org/exposure/59a44249dec83576d97fd3fce46ec5f9/niederer_smith_2007.cellml/cellml_math
G_SER = 0.45      # SERCA maximal conductance, uM/ms
K_SER = 0.5       # SERCA dissociation constant, uM
# calculating leak to match SERCA uptake rate at rest
JLEAK = G_SER*C_Ca_rest**2/(K_SER**2+C_Ca_rest**2)


