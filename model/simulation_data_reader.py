# !/usr/bin python3
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2019-2022
#  Institute for Experimental Medical Research,
#  Oslo University Hospital and University of Oslo, Oslo, Norway
#  Authors: Yufeng Hou and Martin Laasmaa
#  This file is part of project: Sparks-Simulator
#

import numpy as np
import h5py
from fenics import *
from mshr import *
import pylab as plt
from scipy.ndimage import gaussian_filter
from scipy.interpolate import interp1d

from utils import (cartesian_coordinates, evaluate_fenics_function, print_nodal_values,
                   progress_bar)
from global_parameters import *


class SimulationConverter(object):

    @staticmethod
    def save_data(filename, data, solution_type='F_Ca', attrs=None):
        results_file = h5py.File(filename, 'a')
        for i, d in enumerate(data):
            dname = '{:08d}'.format(i)
            dset = results_file.create_dataset(solution_type+'/'+dname, data=d)
            if attrs is not None:
                for key, val in attrs.items():
                    dset.attrs.create(key, val)

        results_file.close()
        print('File saved:', filename)

    @staticmethod
    def load_data(filename, solution_type='F_Ca'):
        results_file = h5py.File(filename, 'r')
        grp = results_file[solution_type]
        keys = list(grp.keys())
        data = np.zeros((len(keys), *grp[keys[0]].shape))
        attrs = dict(grp[keys[0]].attrs)
        for i, key in enumerate(keys):
            data[i] = grp[key][()]
        results_file.close()
        return data, attrs

    def __init__(self, filename):
        self.filename = filename
        self._read()

    def _read(self):
        h = h5py.File(self.filename, 'r')
        self.data_attrs = dict(h['Configuration'].attrs)
        h.close()

        mesh = Mesh()
        self._data = HDF5File(mesh.mpi_comm(), self.filename, 'r')
        self._data.read(mesh, '/mesh', False)
        self.V = FunctionSpace(mesh, eval(self.data_attrs['mesh_family']), int(self.data_attrs['mesh_degree']))

    def convolve(self, a, voxel, fwhm):
        '''
        a : input image (t, y, x) or (t, z, y, x)
        voxel: (pz, py, px) imput image voxel size in microns
        fwhm : full width at half max of PSF in microns (z, y, x)
        '''
        fwhm2sigma = lambda x: x/(2*np.sqrt(2*np.log(2)))
        sigma = (np.array([fwhm2sigma(_f) for _f in fwhm])/np.array(voxel))[-a.ndim:]
        gaussian_filter(a, sigma, order=0, output=a, mode='nearest', truncate=4.0)

    def convert_to_vtk(self, exposure_time, solution_type='F_Ca'):
        attrs = self.data_attrs
        dt = attrs['dt']
        t0 = attrs['t0']
        t1 = attrs['t1']
        solution_labels = eval(attrs['solution labels'])
        try:
            # solution index
            si = solution_labels.index(solution_type)
        except ValueError:
            print('No such solution type as {}. Available types are: {}'.format(solution_type, attrs['solution labels']))
            raise ValueError('No such solution type')

        save_vtk = './simulated-data/vtks/test'
        vtkfile = File(os.path.join(save_vtk, 'u{}_{}.pvd'.format(si, solution_type)))

        f = self._data
        u = Function(self.V)
        us = Function(self.V)
        out = []
        t = 0
        norm = 0
        num_steps = int(round((t1-t0)/dt))
        for n in range(num_steps):
            t += dt
            f.read(u, '/u{}/values_{:08d}'.format(si,n))
            us = us + u
            progress_bar(n+1, num_steps, prefix='Converting:', suffix='done')
            norm += 1
            if t+dt-exposure_time > 0.001:
                _us = project(us, self.V)
                #us = us/norm
                # res = evaluate_fenics_function(_us, coords)
                # res /= norm
                vtkfile << (u, n*dt)

                del _us

                t = 0
                us = Function(self.V)
                norm = 0


    def integrate(self, exposure_time, solution_type='F_Ca', voxel=(0.1, 0.05, 0.05), binning=1, render_3d=False, convolution_fwhm=None):
        '''
        exposure_time: in milliseconds
        voxel: (pz, py, px) voxel size in microns
        solution_type: output type F_Ca - dye bound calcium, Ca - free calcium
        '''
        attrs = self.data_attrs
        dt = attrs['dt']
        t0 = attrs['t0']
        t1 = attrs['t1']
        solution_labels = eval(attrs['solution labels'])
        try:
            # solution index
            si = solution_labels.index(solution_type)
        except ValueError:
            print('No such solution type as {}. Available types are: {}'.format(solution_type, attrs['solution labels']))
            raise ValueError('No such solution type')

        pz, py, px = voxel
        xbounds = eval(attrs['space xbounds'])
        ybounds = eval(attrs['space ybounds'])
        zbounds = eval(attrs['space zbounds'])
        coords = cartesian_coordinates(xbounds, ybounds, zbounds, px, py, pz)

        f = self._data
        u = Function(self.V)
        us = Function(self.V)
        out = []
        t = 0
        norm = 0
        num_steps = int(round((t1-t0)/dt))
        for n in range(num_steps):
            t += dt
            f.read(u, '/u{}/values_{:08d}'.format(si,n))
            us = us + u
            progress_bar(n+1, num_steps, prefix='Converting:', suffix='done')
            norm += 1
            if t+dt-exposure_time > 0.001:
                _us = project(us, self.V)
                res = evaluate_fenics_function(_us, coords)
                res /= norm
                del _us

                if render_3d:
                    res = self.render3d(res)

                if convolution_fwhm is not None:
                    self.convolve(res, voxel, convolution_fwhm)

                out.append(res)
                t = 0
                us = Function(self.V)
                norm = 0

        return out

    def render3d(self, a):
        hy, hx = [int(np.floor(el/2)+1) for el in a.shape]
        sec = a[-hy:,-hx:] + a[:hy,:hx][::-1,::-1] +\
              a[-hy:,:hx][:,::-1] + a[:hy,-hx:][::-1,:]
        sec *= 0.25
        inds = np.arange(0, hx)
        f0 = sec[int(hy/2),-1]
        quarter = f0*np.ones((hy, hy, hx))
        quarter[0] = sec

        for j in range(hy):
            f = interp1d(inds, sec[j,:])
            for k in range(1, hy):
                for i in range(hx):
                    dx = np.sqrt(k*k+i*i)
                    if dx <= hy-1:
                        quarter[k,j,i] = f(dx)

        new = np.zeros((2*hy-1, 2*hy-1, 2*hx-1))
        new[-hy:, -hy:, -hx:] = quarter
        new[-hy:, :hy , -hx:] = quarter[:,::-1,:]
        new[:hy , -hy:, -hx:] = quarter[::-1,:,:]
        new[:hy , :hy , -hx:] = quarter[::-1,::-1,:]
        new[-hy:, -hy:, :hx] = quarter[:,:,::-1]
        new[-hy:, :hy , :hx] = quarter[:,::-1,::-1]
        new[:hy , -hy:, :hx] = quarter[::-1,:,::-1]
        new[:hy , :hy , :hx] = quarter[::-1,::-1,::-1]
        return new

    def close(self):
        self._data.close()


if __name__ == '__main__':

    import argparse, os

    # patch analysis
    if 0:
        voxel = (0.065, 0.065, 0.065) # (z, y, x)
        integration_dt = 2
        render_3d = True
        convolution_fwhm = FWHM_TIRF_63X
        flux_type = 'lee'
        root = '/media/martin/Seagate Expansion Drive/spark-simulations/fem-simulations/buffering_comparison/%s/'%buffering_type


        for buffering_type in ['ml2', 'kong', 'vijay']:
            for jamp in [1, 5, 10, 15, 20, 25, 30, 50]:
                fn = '%s_J%i_100ms_10us.h5'%(flux_type, jamp)
                src_file = os.path.join(root, fn)
                out_file = os.path.join(root, 'integrated/3d-tirf', fn[:-3]+'_integ-dt%.1fms.h5'%integration_dt)

                obj = SimulationConverter(src_file)
                results_file = h5py.File(out_file, 'w')
                results_file.close()

                for solution_type in ['F_Ca']:#, 'Ca']:
                    d = obj.integrate(integration_dt, solution_type=solution_type, voxel=voxel,
                                      render_3d=render_3d, convolution_fwhm=convolution_fwhm)
                    SimulationConverter.save_data(out_file, d, solution_type,
                                                  attrs={'element_size_um': voxel,
                                                         'integration step': integration_dt,
                                                         'convolution fwhm (z, y, z) [um]': 0 if convolution_fwhm is None else convolution_fwhm,
                                                  }
                    )
                obj.close()

        exit()

    parser = argparse.ArgumentParser(description='Converts simulations to microscope recordings')
    parser.add_argument('simulation_file', nargs=1, help='FEM simulation file')
    parser.add_argument('-o', '--out_path', help='Out put path of result file')
    parser.add_argument('-dt', '--integration_dt', type=float, default=0.5, help='Integration time step')
    parser.add_argument('-3d', '--render_3d', action='store_true', help='Uses 2D and renders it in 3D around the axis of symmetry')
    args = parser.parse_args()

    src_file = args.simulation_file[0]
    voxel = (0.065, 0.065, 0.065) # (z, y, x)
    integration_dt = args.integration_dt
    render_3d = args.render_3d

    obj = SimulationConverter(src_file)
    ofile = os.path.split(src_file)[-1][:-3]+'_integ-dt%.1fms.h5'%integration_dt
    ofile = '3d_'+ofile if args.render_3d else ofile
    if args.out_path is None:
        out_file = os.path.join(os.path.dirname(src_file), 'integrated', ofile)
    else:
        out_file = os.path.join(args.out_path, ofile)

    results_file = h5py.File(out_file, 'w')
    results_file.close()
    convolution_fwhm = FWHM_TIRF_63X

    # for solution_type in ['Ca', 'F_Ca', 'CAL_Ca', 'ATP_Ca', 'TRP_Ca']:
    for solution_type in ['F_Ca']:#, 'Ca']:
        d = obj.integrate(integration_dt, solution_type=solution_type, voxel=voxel,
                          render_3d=render_3d, convolution_fwhm=convolution_fwhm)
        SimulationConverter.save_data(out_file, d, solution_type,
                                      attrs={'element_size_um': voxel,
                                             'integration step [ms]': integration_dt,
                                             'convolution fwhm (z, y, z) [um]': 0 if convolution_fwhm is None else convolution_fwhm,
                                      }
        )
    obj.close()
