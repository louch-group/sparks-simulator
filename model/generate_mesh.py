# !/usr/bin/env python3
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2019-2022
#  Institute for Experimental Medical Research,
#  Oslo University Hospital and University of Oslo, Oslo, Norway
#  Authors: Yufeng Hou and Martin Laasmaa
#  This file is part of project: Sparks-Simulator
#

import fenics
import mshr
import pickle
import pylab as plt
import numpy as np

from global_parameters import *


class CircleDomain(fenics.SubDomain):

    def __init__(self, x0, y0, r):
        fenics.SubDomain.__init__(self)
        self.x0 = x0
        self.y0 = y0
        self.r = r

    def inside(self, x, on_boundary):
        xx = x[0] - self.x0
        yy = x[1] - self.y0
        return fenics.sqrt(xx*xx+yy*yy) < self.r+1e-8


class RyRMesher(object):

    def __init__(self, xbounds, ybounds, zbounds, points,
                 point_radius=0.3, grade=32, iterations=3):

        self.xbounds = xbounds
        self.ybounds = ybounds
        self.zbounds = zbounds
        self.points = points
        self.r = point_radius
        self.grade = grade

        self.domain = mshr.Rectangle(fenics.Point(self.xbounds[0], self.ybounds[0]),
                                     fenics.Point(self.xbounds[1], self.ybounds[1]))

        for i, pnt in enumerate(self.points):
            sd = mshr.Circle(fenics.Point(*pnt), self.r)
            self.domain.set_subdomain(i+1, sd)

        mesh = mshr.generate_mesh(self.domain, self.grade)
        cc = fenics.MeshFunction('bool', mesh, False)
        for pnt in self.points:
            print(pnt)
            CircleDomain(*pnt, self.r).mark(cc, True)
            CircleDomain(*pnt, 0.5*self.r).mark(cc, True)
            CircleDomain(*pnt, 0.25*self.r).mark(cc, True)

        for i in range(iterations):
            mesh = fenics.refine(mesh, cc)
        self.mesh = mesh

    def save(self, filename):
        ''' to read:
        mesh = Mesh()
        mesh_file = HDF5File(mesh.mpi_comm(), mesh_name+'.h5', 'r')
        mesh_file.read(mesh, mesh_name, False)
        '''
        fn = filename if filename.endswith('.h5') else filename+'.h5'
        f = fenics.HDF5File(self.mesh.mpi_comm(), fn, 'w')
        f.write(self.mesh, fn.split('/')[-1])

    def plot(self):
        fenics.plot(self.mesh)


def load_pkl(name):
    with open(name, 'rb') as f:
        return pickle.load(f)


def get_points():
    # ryr_data = load_pkl('./test_data/position_data.pkl')
    # inds = ryr_data['posz'] == 5
    # x = ryr_data['posx'][inds]
    # y = ryr_data['posy'][inds]

    # inds = (x<10)*(x>5)*(y<10)*(y>5)
    # x = x[inds] - 5
    # y = y[inds] - 5

    return np.array([[3,3]])
    # return np.array([[i, j] for i, j in zip(x, y)])


if __name__ == '__main__':

    points = get_points() # release site in the middle of area
    xbounds = 0, 6
    ybounds = 0, 6
    zbounds = 0, 5

    m = RyRMesher(xbounds, ybounds, zbounds, points, grade=32, iterations=1)
    m.save('./test_data/small_test_mesh_2D_grade32')
    m.plot()
    plt.scatter(points[:,0], points[:,1])
    circle2 = plt.Circle((points[:,0], points[:,1]), PS_RADIUS, color='r', fill=False)
    plt.gcf().gca().add_artist(circle2)
    plt.show()
