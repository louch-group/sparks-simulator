
V_SS = 1.0e-12 # uL volume of subspace from lee ... sobie 2012
V_ME = 4/3*7.5e-12 # uL volume of the first element 4/3 shpere/circle vol/surface

# MODEL spesific parameters
PS_RADIUS = 0.05 # radius of point source, micros

# PSF
# FWHM z, y, x microns, from https://svi.nl/NyquistCalculator
FWHM_CONFOCAL_60X = (0.47558, 0.21875, 0.21875) # confocal, objective 60x, NA 1.3, ex 488nm, em 520, water immersion
FWHM_WIDEFIELD_60X = (0.76683, 0.30000, 0.30000) # confocal, objective 60x, NA 1.3, ex 488nm, em 520, water immersion
FWHM_WIDEFIELD_63X = (0.50000, 0.29592, 0.29592) # confocal, objective 63x, NA 1.3, ex 488nm, em 520, water immersion
FWHM_TIRF_63X = (0.15000, 0.29592, 0.29592) #confocal, objective 63x, NA 1.45, ex 488nm, em 515, oil immersion
