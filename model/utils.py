#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2019-2022
#  Institute for Experimental Medical Research,
#  Oslo University Hospital and University of Oslo, Oslo, Norway
#  Authors: Yufeng Hou and Martin Laasmaa
#  This file is part of project: Sparks-Simulator
#

import os
import h5py
import numpy as np
import time
import pylab as plt

from math import sin, cos, atan, exp, log, sqrt

from scipy.interpolate import LSQUnivariateSpline, interp1d
from scipy.optimize import leastsq, least_squares
from scipy.ndimage import gaussian_filter


def sigma2fwhm(s):
    return 2*sqrt(log(2))*s


def make_gauss(nx, ny, phi, x0, y0, a, sx, sy):
    _x, _y = np.mgrid[0.0:nx, 0.0:ny]
    sigma_X = 2*sx*sx
    sigma_Y = 2*sy*sy
    x = _x - x0
    y = _y - y0
    cs = cos(phi)**2
    sn = sin(phi)**2
    sn2 = 0.5*sin(2*phi)

    _a = cs/sigma_X + sn/sigma_Y
    _b = -sn2/sigma_X + sn2/sigma_Y
    _c = sn/sigma_X + cs/sigma_Y
    return abs(a)*np.exp( -(_a*x*x + _b*2*x*y + _c*y*y)) + 1


def fit_2d_gaussian(arr):
    nx, ny = arr.shape
    _x, _y = np.mgrid[0.0:nx, 0.0:ny]

    def gaussian(phi, x0, y0, a, sx, sy):
        sigma_X = 2*sx*sx
        sigma_Y = 2*sy*sy
        x = _x - x0
        y = _y - y0
        cs = cos(phi)**2
        sn = sin(phi)**2
        sn2 = 0.5*sin(2*phi)

        _a = cs/sigma_X + sn/sigma_Y
        _b = -sn2/sigma_X + sn2/sigma_Y
        _c = sn/sigma_X + cs/sigma_Y
        return abs(a)*np.exp( -(_a*x*x + _b*2*x*y + _c*y*y)) + 1

    def errfunc(v):
        phi, x0, y0, a, sx, sy = v
        res = arr - gaussian(np.arctan(phi)/1.8, x0, y0, a, sx, sy)
        return res.flatten()

    def gaussian_no_angle(x0, y0, a, sx, sy):
        sigma_X = 2*sx*sx
        sigma_Y = 2*sy*sy
        x = _x - x0
        y = _y - y0
        return abs(a)*np.exp( -(x*x/sigma_X  + y*y/sigma_Y)) + 1

    # def errfunc_no_angle(v, im):
    #     x0, y0, a, sx, sy = v
    #     res = im - gaussian_no_angle(x0, y0, a, sx, sy)
    #     return res.flatten()

    def errfunc_no_angle(v, im):
        x0, y0, a, sx, sy = v
        res = (im - gaussian_no_angle(x0, y0, a, sx, sy))/im
        return res.flatten()

    arr_sum = arr.sum()
    _x0 = np.sum(_x*arr)/arr_sum
    _y0 = np.sum(_y*arr)/arr_sum

    arr_blur = gaussian_filter(arr, 1)

    # initial fit
    t0 = time.time()
    #v0 = [_x0, _y0, arr_blur.max()-1, 3, 3]
    a0 = np.percentile(arr_blur, 85)-1
    #v0 = [_x0, _y0, a0, 3, 3]
    v0 = [_x0, _y0, a0, 5, 5]
    r = leastsq(errfunc_no_angle, v0, args=(arr_blur,), factor=0.1)
    #r = least_squares(errfunc_no_angle, v0, args=(arr_blur,), loss='soft_l1', f_scale=0.1)
    status = r[1]#r.status
    sol = r[0]#r.x
    if 1:#status < 1:# != 1:
        print('First step')
        print('\tTime', time.time()-t0)
        print('\t  v0', v0)
        print('\t fit', sol)

    t0 = time.time()
    v0 = [0, *sol]
    #v0 = [0, sol[0], sol[1], a0, sol[2], sol[3]]
    r = leastsq(errfunc, v0, factor=0.1)
    #r = least_squares(errfunc, v0, loss='soft_l1', f_scale=0.1)
    status = r[1]#r.status
    sol = r[0]#r.x
    phi, x0, y0, a, sx, sy = sol
    phi = np.arctan(phi)/1.8
    #status = r.status
    if 1:#status < 1:
        print('Second step')
        print('\tTime', time.time()-t0)
        print('\t  v0', v0)
        print('\t fit', phi, x0, y0, a, sx, sy)

    return phi, x0, y0, abs(a), abs(sx), abs(sy)


def tmp_plot(im, arr, f0, p):

    phi, x0, y0, a, sx, sy = p
    nx, ny = im.shape
    fit_im = make_gauss(nx, ny, phi, x0, y0, a, sx, sy)
    im = im / f0
    fig = plt.figure()
    ax1 = fig.add_subplot(231)
    ax2 = fig.add_subplot(232)
    ax3 = fig.add_subplot(233)
    ax4 = fig.add_subplot(212)

    mx = max(im.max(), arr.max())
    mn = min(im.min(), arr.min())

    ax1.imshow(im, vmin=mn, vmax=mx)
    ax2.imshow(arr, vmin=mn, vmax=mx)
    ax3.imshow(fit_im, vmin=mn, vmax=mx)

    xs = 40
    ax4.plot(im[xs], label='Original')
    ax4.plot(arr[xs], label='Noisy')
    ax4.plot(fit_im[xs], label='Fit')
    ax4.legend(frameon=False)

    plt.show()


def test_fitting(im, f0=1):

    #im = 3*make_gauss(*im.shape, 0.26, 40, 50, 3, 4, 5)
    #f0 = 3

    arr = np.random.poisson(im)# + np.random.normal(0, 0.4, im.shape)
    arr = arr / f0

    phi, x0, y0, a, sx, sy = fit_2d_gaussian(arr)
    nx, ny = im.shape
    fit_im = make_gauss(nx, ny, phi, x0, y0, a, sx, sy)

    params = np.array([fit_2d_gaussian(np.random.poisson(im)/f0) for i in range(20)])

    im = im / f0

    fig = plt.figure()
    ax1 = fig.add_subplot(331)
    ax2 = fig.add_subplot(332)
    ax3 = fig.add_subplot(333)
    ax4 = fig.add_subplot(312)

    mx = max(im.max(), arr.max())
    mn = min(im.min(), arr.min())

    ax1.imshow(im, vmin=mn, vmax=mx)
    ax2.imshow(arr, vmin=mn, vmax=mx)
    ax3.imshow(fit_im, vmin=mn, vmax=mx)

    xs = 40
    ax4.plot(im[xs], label='Original')
    ax4.plot(arr[xs], label='Noisy')
    ax4.plot(fit_im[xs], label='Fit')
    ax4.legend(frameon=False)

    for i, label in enumerate(['phi', 'x0', 'y0', 'a', 'sx', 'sy']):
        ax = fig.add_subplot(3,6,i+13)
        p = params[:, i]
        if label=='phi':
            p = p/np.pi*180
        ax.hist(p, bins=21, density=True)
        ax.set_xlabel(label)

    for i, p in enumerate(fit_2d_gaussian(im)):
        ax = fig.add_subplot(3,6,i+13)
        print(p)
        if i == 0:
            p = p/np.pi*180
        ax.axvline(p, ls='--', color='k')

    plt.show()


def lee_flux(n_ryr, t0, flux_type, path='./lee_fluxes'):
    fn = os.path.join(path, 'lee_%i.h5'%n_ryr)
    h5 = h5py.File(fn, 'r')
    t = h5['time'][()]
    if flux_type == 'lee_peak':
        c = h5['flux peak'][()]
    elif flux_type == 'lee_mean':
        c = h5['flux mean'][()]
    else:
        raise NotImplementedError(f'Flux type `{flux_type}` is not implemented!')
    h5.close()
    i0 = np.where(c > 0)[0][0]
    t = t[i0:]
    t -= t[0]
    t += t0
    c = c[i0:]

    return interp1d(t, c, bounds_error=False, fill_value=0.0)


def ryr_flux(t, c):
    ''' t: time
        c: current
    '''
    nodes = 6
    dx = t[-1]/nodes
    kn = np.linspace(t[0]+dx/10, t[-1]-dx/10, nodes)
    _t = np.arange(t[0], t[-1], 0.25)
    _c = np.interp(_t, t, c)
    return LSQUnivariateSpline(_t, _c, t[1:-1], k=3, ext=1)


def read_ryr_fluxes(filename):
    with open(filename, 'r') as f:
        d = {els[0]:[float(x) for x in els[1:]] for els in [line.strip().split(',') for line in f.readlines()]}
    return d


def get_ryr_flux_nodes(no_ryr):
    if no_ryr < 10 or no_ryr > 100:
        raise NotImplementedError('Not done yet')

    fluxes = read_ryr_fluxes('ryr_fluxes_sobie_bpj2002.csv')
    known_cluster_fluxes = np.array([10, 20, 50, 100])
    if no_ryr in known_cluster_fluxes:
        return fluxes['%iRyR_x'%no_ryr], fluxes['%iRyR_y'%no_ryr]

    i = np.where(known_cluster_fluxes > no_ryr)[0][0]
    t = np.arange(0, 50, 0.001)

    i1, i2 = known_cluster_fluxes[i-1], known_cluster_fluxes[i]
    x1, y1 = fluxes['%iRyR_x'%i1], fluxes['%iRyR_y'%i1]
    jryr1 = np.nan_to_num(ryr_flux(x1, y1)(t))

    x2, y2 = fluxes['%iRyR_x'%i2], fluxes['%iRyR_y'%i2]
    jryr2 = np.nan_to_num(ryr_flux(x2, y2)(t))
    jryr = (no_ryr-i1)/(i2-i1)*(jryr2-jryr1) + jryr1

    inds = jryr>0
    t0val = t[jryr==0][0]

    return np.append(t[inds][::1000], t0val),  np.append(jryr[inds][::1000], 0)


def nodal_values(u):
    return u.vector().get_local()


def print_nodal_values(u):
    u_nodal_values = nodal_values(u)
    print(u_nodal_values.mean(), u_nodal_values.min(), u_nodal_values.max())


def cartesian_coordinates(xbounds, ybounds, zbounds, px, py, pz):
    c = [np.round(np.arange(xbounds[0], xbounds[1]+px, px), 3),
         np.round(np.arange(ybounds[0], ybounds[1]+py, py), 3),
         np.round(np.arange(zbounds[0], zbounds[1]+pz, pz), 3)]
    if c[0][-1] > xbounds[1]: c[0] = c[0][:-1]
    if c[1][-1] > ybounds[1]: c[1] = c[1][:-1]
    if c[2][-1] > zbounds[1]: c[2] = c[2][:-1]
    return c


def result_array_shape(coords):
    return [el.size for el in coords]


def evaluate_fenics_function(u, coords):
    dim = u.geometric_dimension()
    if dim == 2:
        res = np.zeros(result_array_shape(coords[:2]))
        for i, x in enumerate(coords[0]):
            for j, y in enumerate(coords[1]):
                    res[i,j] = u(x,y)

    elif dim == 3:
        res = np.zeros(result_array_shape(coords))
        for i, x in enumerate(coords[0]):
            for j, y in enumerate(coords[1]):
                for k, z in enumerate(coords[2]):
                    res[i,j,k] = u(x,y,z)

    return res


def progress_bar(iteration, total, prefix='', suffix='', decimals=1, length=25, fill='#'):
    """ Call in a loop to create terminal progress bar
    parameters:
    -----------
    iteration - Required : current iteration (int)
    total     - Required : total iterations (int)
    prefix    - Optional : prefix string (str)
    suffix    - Optional : suffix string (str)
    decimals  - Optional : positive number of decimals in percent complete (int)
    length    - Optional : character length of bar (int)
    fill      - Optional : bar fill character (str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filled_length = int(length * iteration // total)
    bar = fill * filled_length + '-' * (length - filled_length)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')

    # print new line on complete
    if iteration == total:
        print()
