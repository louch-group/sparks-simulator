# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 09:45:49 2019

@author: yufengh
"""

import numpy as np
from skimage import io, filters, morphology
from skimage.feature import peak_local_max
import scipy.ndimage as nd
from mayavi import mlab
import pickle


class CentroidIdentifier:
    def __init__(self):
        self.xscale = None
        self.yscale = None
        self.zscale = None
        self.img = None
        self.ryr = None
        self.size = None
        self.position_size_data = None
        self.probability = None
        self.positions = None
        self.label_map = None
        self.objno = None
        return
    
    @staticmethod
    def _randomise_sizes(probability_array):
        roll = np.random.rand()
        subtraction = np.abs(probability_array[:, 2] - roll)
        ryr = probability_array[np.argmin(subtraction), 0]
        return ryr
    
    @staticmethod
    def _save_obj(obj, fpath):
        with open(fpath, 'wb') as f:
            pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
    
    @staticmethod
    def _load_obj(name):
        with open(name, 'rb') as f:
            return pickle.load(f)
    
    @staticmethod
    def generate_probability_table():
        """
        TODO: need to build this in to generate the npy probability table.
        nRyR|prob|cumprob
        """
        return
        
    def load_image(self, dir, fname):
        self.img = io.imread(dir + fname)
    
    def load_probabilities(self, dir, fname):
        self.probability = np.load(dir + fname)        
        
    def crop_image(self, zcrop = None, ycrop = None, xcrop = None):
        if zcrop:
            self.img = self.img[zcrop[0]: zcrop[1]]
        
        if ycrop:
            self.img = self.img[:, ycrop[0]: ycrop[1]]
            
        if xcrop:
            self.img = self.img[:, :, xcrop[0]: xcrop[1]]
            
        return
        
    def set_scale(self, x, y, z):
        self.xscale = x
        self.yscale = y
        self.zscale = z
        
    def identify_centroid(self, threshold = True, mode = None):
        """
        pinpoint centroids for specific sites
        
        highly recommended to run with threshold, poor results if threshold is not used. 
        Incompatiable if no threshold with EDT or binary modes
        threshold option mostly present incase data is prethreshed
        """
        """
        threshold
        """
        if threshold:
            threshold = filters.threshold_otsu(self.img)
            imgthresh = self.img > threshold
            
        else:
            imgthresh = np.ones(self.img.shape)
        
        if mode == 'edt':
            imgthresh = nd.distance_transform_edt(imgthresh)
            
        elif mode == 'binary':
            imgthresh = imgthresh
            
        else:
            imgthresh = imgthresh * self.img
        
        """
        label locations, find local peaks as centroids
        """
        
        local_peaks = peak_local_max(imgthresh, indices = False)
        labels, nobjs = nd.label(local_peaks.astype(int))
    
    
        centroids = nd.measurements.center_of_mass(local_peaks, labels = labels, index = np.arange(nobjs)+1)
        centroids = np.array(centroids)
        return centroids, labels, nobjs

    def generate_pos_data(self, use_random = True, generate_dict = True, apply_scale = False):
        """
        generates coordinate data and label map, 
        applys probability distribution to determine N receptors in cluster
        will likely be superceeded buy actual measurements from true ryR structural data later.
        """
        self.positions, self.label_map, self.objno = self.identify_centroid(self.img)
        size = []
        
        if use_random:
            if self.probability:
                for i in range(len(self.positions)):
                    size.append(self._randomise_sizes(self.probability))
                    
            else:
                raise ValueError('probability distribution not provided')
            
            self.size = np.array(size)
            
        else:
            """
            TODO: add in specific cluster segmentation via some sort of watershed or something
            """
            pass
        
        if generate_dict:
            self._generate_dict(self, apply_scale = apply_scale)
        
        return
    
    def _apply_scale(self):
        self.positions = self.positions*np.array([[self.zscale, self.zscale, self.zscale]])
        return
        
    def _generate_dict(self, apply_scale = False):
        """
        generate dict for final output
        """
        
        if apply_scale:
            self._apply_scale()
        
            self.position_size_data = {'posz':self.positions[:,0],
                                      'posy':self.positions[:,1],
                                      'posx':self.positions[:,2],
                                      'nRyR':self.size,
                                      'distribution':self.probability,
                                      'zscale':self.zscale,
                                      'yscale':self.yscale,
                                      'xscale':self.xscale,
                                      'img_data': self.img,
                                      'label_map': self.label_map,
                                      'scale_applied':True}
        else:
            self.position_size_data = {'posz':self.positions[:,0],
                                      'posy':self.positions[:,1],
                                      'posx':self.positions[:,2],
                                      'nRyR':self.size,
                                      'distribution':self.probability,
                                      'zscale':self.zscale,
                                      'yscale':self.yscale,
                                      'xscale':self.xscale,
                                      'img_data': self.img,
                                      'label_map': self.label_map,
                                      'scale_applied':False }
    
    def save_position_data(self, outdir):
        if self.position_size_data:
            if outdir.endswith('.pkl'):
                self._save_obj(self.position_size_data, outdir)
            else:
                self._save_obj(self.position_size_data, outdir + '.pkl')
                
        else:
            raise ValueError('position_size_data not yet loaded')
            
    def load_position_data(self, indir):
        """
        TODO: make this sometime later.
        """
        return
    